<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Academy extends Model
{
    protected $table = 'academies';
    public $timestamps = true;

    public function uploads()
    {
        return $this->hasMany('App\Models\AcademyUpload');
    }
}

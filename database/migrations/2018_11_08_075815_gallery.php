<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Gallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_arm')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('title_eng')->nullable();
            $table->string('folder')->nullable();
            $table->string('link')->nullable();
            $table->date('date')->nullable();
            $table->string('active')->nullable();
            $table->string('gallery_upload')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery');
    }
}

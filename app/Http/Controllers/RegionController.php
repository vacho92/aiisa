<?php

namespace App\Http\Controllers;

use App\Models\EventToRegion;
use App\Models\NewsToRegion;
use App\Models\Region;
use App\Models\VideoToRegion;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class RegionController extends Controller
{
    public function Regions()
    {
        $regions = Region::get();
        return view('pages/regions',['region'=>$regions]);
    }
    public function addRegions(Request $request)
    {
        if ($request->isMethod('get')){

            return view('pages/addRegions');
        }elseif($request->isMethod('post')){
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $region = new Region;
            $region->name_arm = $name_arm;
            $region->name_ru = $name_ru;
            $region->name_eng = $name_eng;
            $region->desc_arm = $desc_arm;
            $region->desc_ru = $desc_ru;
            $region->desc_eng = $desc_eng;
            $region->active = $active;
            $region->save();
            return redirect('admin/regions')->with('success','Successfully added');
        }

    }
    public function editRegion(Request $request,$id)
    {
        if ($request->isMethod('get')){
            $region = Region::find($id);
            return view('pages/editRegions',['region'=>$region]);
        }elseif($request->isMethod('post')){
            //dd($request->toArray());
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $region = Region::find($id);
            //dd($region);
            $region->name_arm = $name_arm;
            $region->name_ru = $name_ru;
            $region->name_eng = $name_eng;
            $region->desc_arm = $desc_arm;
            $region->desc_ru = $desc_ru;
            $region->desc_eng = $desc_eng;
            $region->active = $active;
            $region->save();
        }

        return redirect('admin/regions')->with('success','Successfully edited');

    }
    public function deleteRegion($id)
    {
        Region::where('id',$id)->delete();
        VideoToRegion::where('region_id',$id)->delete();
        NewsToRegion::where('region_id',$id)->delete();
        EventToRegion::where('region_id',$id)->delete();
        return Redirect::back();
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderUpload extends Model
{
    protected $table = 'slider_upload';
    public $timestamps = true;
}

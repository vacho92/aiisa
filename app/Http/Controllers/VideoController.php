<?php

namespace App\Http\Controllers;

use App\Models\AuthorsExperts;
use App\Models\Project;
use App\Models\Region;
use App\Models\Topic;
use App\Models\User;
use App\Models\Video;
use App\Models\VideoAuthors;
use App\Models\VideoToProject;
use App\Models\VideoToRegion;
use App\Models\VideoToTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VideoController extends Controller
{
    public function Videos()
    {
        $videos = Video::paginate(10);
        return view('pages/videos', ['data' => $videos]);
    }

    public function addVideos(Request $request)
    {
        if ($request->isMethod('get')) {

            $authors = AuthorsExperts::get();
            $topic = Topic::get();
            $region = Region::get();
            $project = Project::get();

            return view('pages/addVideos', ['authors' => $authors, 'topic' => $topic, 'region' => $region, 'project' => $project]);
        } elseif ($request->isMethod('post')) {

            $err = false;
            $errors = array();
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $link = $request->input('link');
            $media_link_arm = $request->input('media_link_arm');
            $media_link_ru = $request->input('media_link_ru');
            $media_link_eng = $request->input('media_link_eng');
            $video_date = $request->input('date');
            $author = $request->input('author');
            $active = $request->input('active');
            $topics_select = $request->input('topics');
            $region_select = $request->input('region');
            $program = $request->input('program');

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
           /* if (!$media_link_arm && !$media_link_ru && !$media_link_eng) {
                $errors['media_link'] = "At least one of Media Link  required";
                $err = true;
            }*/
            if (!$author) {
                $errors['author'] = "Author field required";
                $err = true;
            }
            if (!$video_date) {
                $errors['video_date'] = "Date  field required";
                $err = true;
            }
            if (!$link) {
                $errors['link'] = "Link  field required";
                $err = true;
            }
            /*if (!$topics_select) {
                $errors['topic'] = "Topic field required";
                $err = true;
            }
            if (!$region_select) {
                $errors['region'] = "Region field required";
                $err = true;
            }
            if (!$program) {
                $errors['program'] = "Program field required";
                $err = true;
            }*/
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $insert = new Video;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->media_link_arm = $media_link_arm;
            $insert->media_link_ru = $media_link_ru;
            $insert->media_link_eng = $media_link_eng;
            $insert->author = $author;
            $insert->date = $video_date;
            $insert->link = $link;
            $insert->active = $active;
            $insert->save();
            if($topics_select){
                foreach ($topics_select as $value) {
                    $insert_topics = new VideoToTopic;
                    $insert_topics->topic_id = $value;
                    $insert_topics->video_id = $insert->id;
                    $insert_topics->save();
                }
            }
            if($region_select){
                foreach ($region_select as $value) {
                    $insert_region = new VideoToRegion;
                    $insert_region->region_id = $value;
                    $insert_region->video_id = $insert->id;
                    $insert_region->save();
                }
            }
            if($program){
                foreach ($program as $value) {
                    $insert_project = new VideoToProject;
                    $insert_project->project_id = $value;
                    $insert_project->video_id = $insert->id;
                    $insert_project->save();
                }
            }

            return redirect('admin/videos')->with('success', 'Successfully added');
        }

    }

    public function editVideos(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            $video = Video::where('id', $id)
                ->with('topics')
                ->with('region')
                ->with('project')
                ->with('authors')
                ->get()->first();
//            dd($video->toArray());
            $authors = AuthorsExperts::get();
            $topic = Topic::get();
            $region = Region::get();
            $project = Project::get();

            return view('pages/editVideos', [
                'data' => $video,
                'authors' => $authors,
                'topic' => $topic,
                'region' => $region,
                'project' => $project]);
        } elseif ($request->isMethod('post')) {

            $err = false;
            $errors = array();
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $link = $request->input('link');
            $media_link_arm = $request->input('media_link_arm');
            $media_link_ru = $request->input('media_link_ru');
            $media_link_eng = $request->input('media_link_eng');
            $video_date = $request->input('date');
            $author = $request->input('author');
            $active = $request->input('active');
            $topics_select = $request->input('topics');
            $region_select = $request->input('region');
            $program = $request->input('program');
            $authors = $request->input('authors');
            //dd($request->toArray());
            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            /*if (!$media_link_arm && !$media_link_ru && !$media_link_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }*/
            if (!$author) {
                $errors['author'] = "Author field required";
                $err = true;
            }
            if (!$video_date) {
                $errors['video_date'] = "Date  field required";
                $err = true;
            }
            if (!$link) {
                $errors['link'] = "Link  field required";
                $err = true;
            }
           /* if (!$topics_select) {
                $errors['topic'] = "Topic field required";
                $err = true;
            }
            if (!$region_select) {
                $errors['region'] = "Region field required";
                $err = true;
            }
            if (!$program) {
                $errors['program'] = "Program field required";
                $err = true;
            }*/
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $update = Video::find($id);
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->media_link_arm = $media_link_arm;
            $update->media_link_ru = $media_link_ru;
            $update->media_link_eng = $media_link_eng;
            $update->author = $author;
            $update->date = $video_date;
            $update->link = $link;
            $update->active = $active;
            $update->save();
            VideoToTopic::where('video_id', $id)->delete();
            if($topics_select){
                foreach ($topics_select as $value) {
                    $insert_topics = new VideoToTopic;
                    $insert_topics->topic_id = $value;
                    $insert_topics->video_id = $id;
                    $insert_topics->save();
                }
            }
            VideoToRegion::where('video_id', $id)->delete();
            if($region_select){
                foreach ($region_select as $value) {
                    $insert_region = new VideoToRegion;
                    $insert_region->region_id = $value;
                    $insert_region->video_id = $id;
                    $insert_region->save();
                }
            }
            VideoToProject::where('video_id', $id)->delete();
            if($program){
                foreach ($program as $value) {
                    $insert_project = new VideoToProject;
                    $insert_project->project_id = $value;
                    $insert_project->video_id = $id;
                    $insert_project->save();
                }
            }
            VideoAuthors::where('video_id', $id)->delete();
            if($authors){
                foreach ($authors as $value) {
                    $insert_project = new VideoAuthors;
                    $insert_project->author_id = $value;
                    $insert_project->video_id = $id;
                    $insert_project->save();
                }
            }

            return redirect('admin/videos')->with('success', 'Successfully edited');
        }

    }

    public function deleteVideos($id)
    {
        VideoToProject::where('video_id',$id)->delete();
        VideoToRegion::where('video_id',$id)->delete();
        VideoToTopic::where('video_id',$id)->delete();
        VideoAuthors::where('video_id',$id)->delete();
        Video::where('id',$id)->delete();
        return redirect('admin/videos')->with('success', 'Successfully deleted');


    }


}

@extends('pages.home')
@section('home')
    <div>
        <h1>Edit Events </h1>
    </div>
    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post"
              action="{{url('admin/edit_event/'.$data['id'])}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-5">
                    <select name="status" id="country" class="form-control">
                        <option class="form-control" @if($data['status']==='public') selected @endif value="public">
                            public
                        </option>
                        <option class="form-control" @if($data['status']==='private') selected @endif value="private">
                            private
                        </option>
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("status",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['status'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_arm']}}" name="title_arm"
                           placeholder="Title">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_ru']}}" name="title_ru"
                           placeholder="Title">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_eng']}}" name="title_eng"
                           placeholder="Title">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("title",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['title'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Event type</label>
                <div class="col-sm-5">
                    <select name="event_types" id="country" class="form-control">
                        @foreach($eventType as $value)
                            @if($value)
                                <option class="form-control" value="{{$value['id']}}"
                                        @if($data['event_type']==$value['id'])selected @endif >
                                    @if($value['name_arm']){{$value['name_arm']}}
                                    @elseif($value['name_ru']){{$value['name_ru']}}
                                    @else{{$value['name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("event_types",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['event_types'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="event_date" class="col-sm-2 control-label">Event Date</label>
                <div class="col-sm-5">
                    <input type="date" class="form-control" value="{{$data['event_date']}}" name="event_date"
                           id="event_date">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("event_date",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['event_date'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="event_from" class="col-sm-2 control-label">From Time</label>
                <div class="col-sm-5">
                    <input type="time" class="form-control" value="{{$data['from_time']}}" name="event_from"
                           id="event_from">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("event_from",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['event_from'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="event_to" class="col-sm-2 control-label">To Time</label>
                <div class="col-sm-5">
                    <input type="time" class="form-control" value="{{$data['to_time']}}" name="event_to" id="event_to">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("event_to",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['event_to'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="location" class="col-sm-2 control-label">Location</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['location']}}" name="location" id="location">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("location",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['location'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="participants" class="col-sm-2 control-label">Participants</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" name="participants_arm" id="participants_arm"
                              placeholder="Participants"> {{$data['participants_arm']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('participants_arm');
                    </script>
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="participants" class="col-sm-2 control-label">Participants</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" name="participants_ru" id="participants_ru"
                              placeholder="Participants"> {{$data['participants_ru']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('participants_ru');
                    </script>
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="participants" class="col-sm-2 control-label">Participants</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" name="participants_eng" id="participants_eng"
                              placeholder="Participants"> {{$data['participants_eng']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('participants_eng');
                    </script>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("participants",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['participants'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="overview" class="col-sm-2 control-label">Overview</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" name="overview_arm"
                              placeholder="Overview"> {{$data['overview_arm']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('overview_arm');
                    </script>
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="overview" class="col-sm-2 control-label">Overview</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" name="overview_ru"
                              placeholder="Overview"> {{$data['overview_ru']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('overview_ru');
                    </script>
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="overview" class="col-sm-2 control-label">Overview</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" name="overview_eng"
                              placeholder="Overview"> {{$data['overview_eng']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('overview_eng');
                    </script>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("overview",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['overview'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Select Topic</label>
                <div class="col-sm-5">
                    <select name="topics[]" id="topics" class="form-control" multiple>
                        @foreach($topic as $value)
                            @if($value)
                                <option value="{{$value['id']}}"
                                        @if($data['topics'])
                                        @foreach($data['topics'] as $event_topic)
                                        @if($event_topic['id']==$value['id']) selected
                                        @endif
                                        @endforeach
                                        @endif>
                                    @if($value['name_arm']){{$value['name_arm']}}
                                    @elseif($value['name_ru']){{$value['name_ru']}}
                                    @else{{$value['name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("topic",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['topic'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="region" class="col-sm-2 control-label">Select Region</label>
                <div class="col-sm-5">
                    <select name="region[]" id="region" class="form-control" multiple>
                        @foreach($region as $value)
                            @if($value)
                                <option value="{{$value['id']}}"
                                        @if($data['region'])
                                        @foreach($data['region'] as $event_region)
                                        @if($event_region['id']==$value['id']) selected
                                        @endif
                                        @endforeach
                                        @endif>
                                    @if($value['name_arm']){{$value['name_arm']}}
                                    @elseif($value['name_ru']){{$value['name_ru']}}
                                    @else{{$value['name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("region",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['region'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="program" class="col-sm-2 control-label">Select Program</label>
                <div class="col-sm-5">
                    <select name="program[]" id="program" class="form-control" multiple>
                        @foreach($project as $value)
                            @if($value)
                                <option value="{{$value['id']}}"
                                        @if($data['project'])
                                        @foreach($data['project'] as $event_project)
                                        @if($event_project['id']==$value['id']) selected
                                        @endif
                                        @endforeach
                                        @endif>
                                    @if($value['name_arm']){{$value['name_arm']}}
                                    @elseif($value['name_ru']){{$value['name_ru']}}
                                    @else{{$value['name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("program",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['program'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-5">
                    <input type="email" class="form-control" value="{{$data['email']}}" name="email" id="inputEmail3" placeholder="Email">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("email",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['email'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-5">
                    <select name="active" class="form-control">
                        <option class="form-control" @if($data['active']==='active') selected @endif value="active"> active</option>
                        <option class="form-control"  @if($data['active']==='inactive') selected @endif value="inactive"> inactive</option>
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("active",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['active'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{url('admin/events')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                </div>
            </div>
        </form>
    </div>

@endsection
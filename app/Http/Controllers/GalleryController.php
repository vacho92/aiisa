<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\GalleryUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    public function Gallery()
    {
        $gallery = Gallery::get();
        return view('pages/gallery', ['data' => $gallery]);
    }

    public function addGallery(Request $request)
    {
        if ($request->isMethod('get')) {

            return view('pages/addGallery');
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            //dd($request->toArray());
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $date = $request->input('date');
            $folder = $request->input('folder');
            $active = $request->input('active');
            $gallery_upload = $request->file('gallery_upload');
            $page_upload = $request->file('upload');
            $gallery_name = '';

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Menu title required";
                $err = true;
            }
            if (!$date) {
                $errors['date'] = "Date field required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($gallery_upload) {
                $gallery_up = array('gallery_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $gallery_up);
                if ($validator->fails()) {
                    $errors['gallery_upload'] = "Gallery image field should be image";
                    $err = true;
                }
                $gallery_name = rand(100, 999) . '_' . time();
                $gallery_name = $gallery_name . '.' . $gallery_upload->getClientOriginalExtension();
                $destinationPath = public_path('/uploads');
                $gallery_upload->move($destinationPath, $gallery_name);
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field  should be image";
                    $err = true;
                }
            }

            if ($err) {
                return Redirect::back()->with('errors', $errors);
            }

            $insert = new Gallery;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->folder = $folder;
            $insert->date = $date;
            $insert->gallery_upload = $gallery_name;
            $insert->active = $active;
            $insert->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new GalleryUpload;
                    $insert_file->gallery_id = $insert->id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/gallery')->with('success', 'Successfully added');
        }

    }

    public function editGallery(Request $request, $id)
    {
        if ($request->isMethod('get')) {

            $gallery = Gallery::where('id', $id)->with('uploads')->get()->first();
            //dd($gallery->toArray());
            return view('pages/editGallery', ['data' => $gallery]);
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            //dd($request->toArray());
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $date = $request->input('date');
            $folder = $request->input('folder');
            $active = $request->input('active');
            $gallery_upload = $request->file('gallery_upload');
            $page_upload = $request->file('upload');
            $gallery_name = null;

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Menu title required";
                $err = true;
            }
            if (!$date) {
                $errors['date'] = "Date field required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($gallery_upload) {
                $gallery_up = array('gallery_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $gallery_up);
                if ($validator->fails()) {
                    $errors['gallery_upload'] = "Gallery image field should be image";
                    $err = true;
                }
                $gallery_name = rand(100, 999) . '_' . time();
                $gallery_name = $gallery_name . '.' . $gallery_upload->getClientOriginalExtension();
                $destinationPath = public_path('/uploads');
                $gallery_upload->move($destinationPath, $gallery_name);
            } else {
                $errors['gallery_upload'] = "Gallery image field required and should be image";
                $err = true;
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field  should be image";
                    $err = true;
                }
            }

            if ($err) {
                return Redirect::back()->with('errors', $errors);
            }

            $update = Gallery::find($id);
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->folder = $folder;
            $update->date = $date;
            $update->gallery_upload = $gallery_name;
            $update->active = $active;
            $update->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new GalleryUpload;
                    $insert_file->gallery_id = $id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/gallery')->with('success', 'Successfully edited');
        }

    }

    public function deleteGalleryTitle($id)
    {
        $gallery = Gallery::find($id);
        if ($gallery) {
            if($gallery->gallery_upload){
                if (File::delete("uploads/" . $gallery->gallery_upload)) {
                    $gallery->gallery_upload = null;
                    $gallery->save();
                }
            }
        }
        return Redirect::back();
    }

    public function deleteGalleryUpload($id)
    {
        $page = GalleryUpload::find($id);
        if ($page) {
            if (File::delete("uploads/" . $page->files)) {
                GalleryUpload::where('id', $id)->delete();
            };
        }
        return Redirect::back();
    }
    public function deleteGallery($id)
    {
        $galleryUpload = GalleryUpload::where('gallery_id', $id)->get();
        if ($galleryUpload) {
            foreach ($galleryUpload as $value) {
                File::delete("uploads/" . $value->files);
            }
            GalleryUpload::where('gallery_id', $id)->delete();
        }
        $gallery = Gallery::find($id);
        if(File::delete("uploads/" . $gallery->gallery_upload)){
            Gallery::where('id', $id)->delete();
        }
        return Redirect::back();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsAuthors extends Model
{
    protected $table = 'news_author';
    public $timestamps = true;
}

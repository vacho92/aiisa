<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventJoin extends Model
{
    protected $table = 'event_join';
    public $timestamps = true;

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SliderUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_upload', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('slider_id')->unsigned()->index();
            $table->foreign('slider_id')->references('id')->on('slider')->onDelete('cascade');
            $table->string('files');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_upload');
    }
}

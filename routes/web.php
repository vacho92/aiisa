<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {

    return view('login');
});

Route::match(['get', 'post'],'/login','HomeController@login');
Route::group(['prefix' => 'admin','middleware' => 'admin'], function(){
    Route::get('/deletePageUpload/{id}', 'PageController@deletePageUpload');
    Route::get('/deletePage/{id}', 'PageController@deletePage');
    Route::get('/deleteSliderUpload/{id}', 'SliderController@deleteSliderUpload');
    Route::get('/deleteSlider/{id}', 'SliderController@deleteSlider');
    Route::get('/deleteGalleryTitle/{id}', 'GalleryController@deleteGalleryTitle');
    Route::get('/deleteGallery/{id}', 'GalleryController@deleteGallery');
    Route::get('/deleteGalleryUpload/{id}', 'GalleryController@deleteGalleryUpload');
    Route::get('/deleteAcademyTitle/{id}', 'AcademiesController@deleteAcademyTitle');
    Route::get('/deleteAcademyUpload/{id}', 'AcademiesController@deleteAcademyUpload');
    Route::get('/deleteAcademies/{id}', 'AcademiesController@deleteAcademies');
    Route::get('/deleteLibraryUpload/{id}', 'LibraryController@deleteLibraryUpload');
    Route::get('/deleteLibrary/{id}', 'LibraryController@deleteLibrary');
    Route::get('/downloadLibraryUpload/{id}', 'LibraryController@downloadLibraryUpload');
    Route::get('/deleteExperts/{id}', 'HomeController@deleteExperts');
    Route::get('/deleteRegion/{id}', 'RegionController@deleteRegion');
    Route::get('/deleteTopic/{id}', 'TopicController@deleteTopic');
    Route::get('/deletePublicationType/{id}', 'PublicationController@deletePublicationType');
    Route::get('/deleteProject/{id}', 'ProjectController@deleteProject');
    Route::get('/deleteEvent/{id}', 'EventController@deleteEvent');
    Route::get('/deleteEventType/{id}', 'EventController@deleteEventType');
    Route::get('/deleteNews/{id}', 'NewsController@deleteNews');
    Route::get('/deleteVideos/{id}', 'VideoController@deleteVideos');
    Route::get('/deleteEmailSubs/{id}', 'HomeController@deleteEmailSubs');
    Route::get('/home', 'HomeController@Dashboard');
    Route::get('/subscription', 'HomeController@EmailSubscription');
    Route::get('/experts', 'HomeController@Experts');
    Route::get('/regions', 'RegionController@Regions');
    Route::get('/topics', 'TopicController@Topics');
    Route::get('/publicationType', 'PublicationController@PublicationType');
    Route::get('/projects', 'ProjectController@Projects');
    Route::get('/events', 'EventController@Events');
    Route::get('/eventsType', 'EventController@EventsType');
    Route::get('/news',  'NewsController@News');
    Route::get('/videos',  'VideoController@Videos');
    Route::get('/pages',  'PageController@Pages');
    Route::get('/slider',  'SliderController@Slider');
    Route::get('/gallery',  'GalleryController@Gallery');
    Route::get('/academies',  'AcademiesController@Academies');
    Route::get('/libraries',  'LibraryController@Library');
    Route::get('/deleteVideos/{id}',  'VideoController@deleteVideos');
    Route::match(['get', 'post'],'/add_experts', 'HomeController@addExperts');
    Route::match(['get', 'post'],'/add_regions', 'RegionController@addRegions');
    Route::match(['get', 'post'],'/add_topics', 'TopicController@addTopics');
    Route::match(['get', 'post'],'/add_publicationType', 'PublicationController@addPublicationType');
    Route::match(['get', 'post'],'/add_projects', 'ProjectController@addProjects');
    Route::match(['get', 'post'],'/add_events', 'EventController@addEvents');
    Route::match(['get', 'post'],'/add_events_type', 'EventController@addEventsType');
    Route::match(['get', 'post'],'/add_news', 'NewsController@addNews');
    Route::match(['get', 'post'],'/add_videos', 'VideoController@addVideos');
    Route::match(['get', 'post'],'/add_pages', 'PageController@addPages');
    Route::match(['get', 'post'],'/add_slider', 'SliderController@addSlider');
    Route::match(['get', 'post'],'/add_gallery', 'GalleryController@addGallery');
    Route::match(['get', 'post'],'/add_academies', 'AcademiesController@addAcademies');
    Route::match(['get', 'post'],'/add_libraries', 'LibraryController@addLibrary');
    Route::match(['get', 'post'],'/edit_region/{id}', 'RegionController@editRegion');
    Route::match(['get', 'post'],'/edit_publicationType/{id}', 'PublicationController@editPublicationType');
    Route::match(['get', 'post'],'/edit_topic/{id}', 'TopicController@editTopic');
    Route::match(['get', 'post'],'/edit_project/{id}', 'ProjectController@editProject');
    Route::match(['get', 'post'],'/edit_event_type/{id}', 'EventController@editEventType');
    Route::match(['get', 'post'],'/edit_event/{id}', 'EventController@editEvents');
    Route::match(['get', 'post'],'/edit_news/{id}', 'NewsController@editNews');
    Route::match(['get', 'post'],'/edit_videos/{id}', 'VideoController@editVideos');
    Route::match(['get', 'post'],'/edit_pages/{id}', 'PageController@editPages');
    Route::match(['get', 'post'],'/edit_slider/{id}', 'SliderController@editSlider');
    Route::match(['get', 'post'],'/edit_gallery/{id}', 'GalleryController@editGallery');
    Route::match(['get', 'post'],'/edit_academy/{id}', 'AcademiesController@editAcademies');
    Route::match(['get', 'post'],'/edit_library/{id}', 'LibraryController@editLibrary');
    Route::match(['get', 'post'],'/edit_experts/{id}', 'HomeController@editExperts');


    Route::get('/logout', 'HomeController@logout');
});


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoToTopic extends Model
{
    protected $table = 'video_topic';
    public $timestamps = true;
}

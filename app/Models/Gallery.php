<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';
    public $timestamps = true;

    public function uploads()
    {
        return $this->hasMany('App\Models\GalleryUpload');
    }
}

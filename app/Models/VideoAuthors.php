<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoAuthors extends Model
{
    protected $table = 'video_author';
    public $timestamps = true;
}

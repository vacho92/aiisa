<?php

namespace App\Http\Controllers;

use App\Models\Academy;
use App\Models\AcademyUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AcademiesController extends Controller
{

    public function Academies()
    {
        $academy = Academy::get();
        return view('pages/academies', ['data' => $academy]);
    }

    public function addAcademies(Request $request)
    {
        if ($request->isMethod('get')) {

            return view('pages/addAcademies');
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $intro_arm = $request->input('intro_arm');
            $intro_ru = $request->input('intro_ru');
            $intro_eng = $request->input('intro_eng');
            $content_arm = $request->input('content_arm');
            $content_ru = $request->input('content_ru');
            $content_eng = $request->input('content_eng');
            $active = $request->input('active');
            $photo_title_arm = $request->input('photo_title_arm');
            $photo_title_ru = $request->input('photo_title_ru');
            $photo_title_eng = $request->input('photo_title_eng');
            $photo = $request->file('photo');
            $page_upload = $request->file('upload');
            $photo_name = '';

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            if (!$name_arm && !$name_ru && !$name_eng) {
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if (!$intro_arm && !$intro_ru && !$intro_eng) {
                $errors['intro'] = "At least one of Intro  text required";
                $err = true;
            }
            if (!$content_arm && !$content_ru && !$content_eng) {
                $errors['content'] = "At least one of Content required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($photo) {
                $gallery_up = array('photo.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $gallery_up);
                if ($validator->fails()) {
                    $errors['photo'] = "Photo title image field should be image";
                    $err = true;
                }
                $photo_name = rand(100, 999) . '_' . time();
                $photo_name = $photo_name . '.' . $photo->getClientOriginalExtension();
                $destinationPath = public_path('/uploads');
                $photo->move($destinationPath, $photo_name);
            } else {
                $errors['photo_title'] = "Photo title image required";
                $err = true;
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field  should be image";
                    $err = true;
                }
            }

            if ($err) {
                return Redirect::back()->with('errors', $errors);
            }

            $insert = new Academy;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->name_arm = $name_arm;
            $insert->name_ru = $name_ru;
            $insert->name_eng = $name_eng;
            $insert->intro_arm = $intro_arm;
            $insert->intro_ru = $intro_ru;
            $insert->intro_eng = $intro_eng;
            $insert->content_arm = $content_arm;
            $insert->content_ru = $content_ru;
            $insert->content_eng = $content_eng;
            $insert->photo = $photo_name;
            $insert->photo_title_arm = $photo_title_arm;
            $insert->photo_title_ru = $photo_title_ru;
            $insert->photo_title_eng = $photo_title_eng;
            $insert->active = $active;
            $insert->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new AcademyUpload;
                    $insert_file->academy_id = $insert->id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/academies')->with('success', 'Successfully added');
        }

    }

    public function editAcademies(Request $request, $id)
    {
        if ($request->isMethod('get')) {

            $academy = Academy::where('id', $id)->with('uploads')->get()->first();
//            dd($academy->toArray());
            return view('pages/editAcademies', ['data' => $academy]);
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            //dd($request->toArray());
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $intro_arm = $request->input('intro_arm');
            $intro_ru = $request->input('intro_ru');
            $intro_eng = $request->input('intro_eng');
            $content_arm = $request->input('content_arm');
            $content_ru = $request->input('content_ru');
            $content_eng = $request->input('content_eng');
            $active = $request->input('active');
            $photo_title_arm = $request->input('photo_title_arm');
            $photo_title_ru = $request->input('photo_title_ru');
            $photo_title_eng = $request->input('photo_title_eng');
            $photo = $request->file('photo');
            $page_upload = $request->file('upload');
            $photo_name = '';

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            if (!$name_arm && !$name_ru && !$name_eng) {
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if (!$intro_arm && !$intro_ru && !$intro_eng) {
                $errors['intro'] = "At least one of Intro  text required";
                $err = true;
            }
            if (!$content_arm && !$content_ru && !$content_eng) {
                $errors['content'] = "At least one of Content required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($photo) {
                $gallery_up = array('photo_title.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $gallery_up);
                if ($validator->fails()) {
                    $errors['photo'] = "Photo title image field should be image";
                    $err = true;
                }
                $photo_name = rand(100, 999) . '_' . time();
                $photo_name = $photo_name . '.' . $photo->getClientOriginalExtension();
                $destinationPath = public_path('/uploads');
                $photo->move($destinationPath, $photo_name);
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field  should be image";
                    $err = true;
                }
            }

            if ($err) {
                return Redirect::back()->with('errors', $errors);
            }

            $update = Academy::find($id);
            $update->name_arm = $name_arm;
            $update->name_ru = $name_ru;
            $update->name_eng = $name_eng;
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->intro_arm = $intro_arm;
            $update->intro_ru = $intro_ru;
            $update->intro_eng = $intro_eng;
            $update->content_arm = $content_arm;
            $update->content_ru = $content_ru;
            $update->content_eng = $content_eng;
            $update->photo_title_arm = $photo_title_arm;
            $update->photo_title_ru = $photo_title_ru;
            $update->photo_title_eng = $photo_title_eng;
            if ($photo_name) {
                $update->photo = $photo_name;
            }
            $update->active = $active;
            $update->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new AcademyUpload;
                    $insert_file->academy_id = $id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/academies')->with('success', 'Successfully edited');
        }

    }

    public function deleteAcademyTitle($id)
    {
        $academy = Academy::find($id);
        if ($academy) {
            if (File::delete("uploads/" . $academy->photo)) {
                $academy->photo = null;
                $academy->save();
            };
        }
        return Redirect::back();
    }

    public function deleteAcademyUpload($id)
    {
        $page = AcademyUpload::find($id);
        if ($page) {
            if (File::delete("uploads/" . $page->files)) {
                AcademyUpload::where('id', $id)->delete();
            };
        }
        return Redirect::back();
    }//deleteAcademies

    public function deleteAcademies($id)
    {
        $academyUpload = AcademyUpload::where('academy_id', $id)->get();
        if ($academyUpload) {
            foreach ($academyUpload as $value) {
                File::delete("uploads/" . $value->files);
            }
            AcademyUpload::where('academy_id', $id)->delete();
        }
        $acadeny = Academy::find($id);

        File::delete("uploads/" . $acadeny->photo);
        Academy::where('id', $id)->delete();

        return Redirect::back();
    }

}

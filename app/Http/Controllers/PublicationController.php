<?php

namespace App\Http\Controllers;

use App\Models\Publication;
use App\Models\PublicationType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PublicationController extends Controller
{
    public function publicationType()
    {
        $publication = Publication::get();
        return view('pages/publicationType',['publication'=>$publication]);
    }
    public function addPublicationType(Request $request)
    {
        if ($request->isMethod('get')){

            return view('pages/addPublicationType');
        }elseif($request->isMethod('post')){
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $public = new Publication;
            $public->name_arm = $name_arm;
            $public->name_ru = $name_ru;
            $public->name_eng = $name_eng;
            $public->desc_arm = $desc_arm;
            $public->desc_ru = $desc_ru;
            $public->desc_eng = $desc_eng;
            $public->active = $active;
            $public->active = $active;
            /* $region->created_at = Carbon::now();
             $region->updated_at = Carbon::now();*/
            $public->save();
            return redirect('admin/publicationType')->with('success','Successfully added');
        }

    }
    public function editPublicationType(Request $request,$id)
    {
        if ($request->isMethod('get')){
            $publication = Publication::find($id);
            return view('pages/editPublicationType',['publication'=>$publication]);
        }elseif($request->isMethod('post')){
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $public = Publication::find($id);
            $public->name_arm = $name_arm;
            $public->name_ru = $name_ru;
            $public->name_eng = $name_eng;
            $public->desc_arm = $desc_arm;
            $public->desc_ru = $desc_ru;
            $public->desc_eng = $desc_eng;
            $public->active = $active;
            $public->save();
            return redirect('admin/publicationType')->with('success','Successfully edited');
        }

    }
    public function deletePublicationType($id)
    {
        PublicationType::where('id',$id)->delete();
        return Redirect::back()->with('success','Successfully deleted');
    }


}

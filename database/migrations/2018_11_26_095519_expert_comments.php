<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpertComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expert_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_arm')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('title_eng')->nullable();
            $table->string('intro_arm')->nullable();
            $table->string('intro_ru')->nullable();
            $table->string('intro_eng')->nullable();
            $table->string('content_arm')->nullable();
            $table->string('content_ru')->nullable();
            $table->string('content_eng')->nullable();
            $table->string('public')->nullable();
            $table->string('documents')->nullable();
            $table->string('photo')->nullable();
            $table->string('photo_title_arm')->nullable();
            $table->string('photo_title_ru')->nullable();
            $table->string('photo_title_eng')->nullable();
            $table->string('photo_by_arm')->nullable();
            $table->string('photo_by_ru')->nullable();
            $table->string('photo_by_eng')->nullable();
            $table->string('date')->nullable();
            $table->string('active')->nullable();
            $table->integer('expert_id')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expert_comments');
    }
}

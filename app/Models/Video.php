<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'video';
    public $timestamps = true;

    public function topics()
    {
        return $this->belongsToMany('App\Models\Topic','video_topic');
    }
    public function region()
    {
        return $this->belongsToMany('App\Models\Region','video_region');
    }
    public function project()
    {
        return $this->belongsToMany('App\Models\Project','video_project');
    }
    public function authors()
    {
        return $this->belongsToMany('App\Models\AuthorsExperts','video_author','video_id','author_id');
    }
}

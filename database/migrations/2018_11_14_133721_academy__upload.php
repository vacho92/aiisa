<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcademyUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academy_upload', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('academy_id')->unsigned()->index();
            $table->foreign('academy_id')->references('id')->on('academies')->onDelete('cascade');
            $table->string('files');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academy_upload');
    }
}

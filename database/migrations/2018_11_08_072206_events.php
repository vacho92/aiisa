<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Events extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->nullable();
            $table->string('title_arm')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('title_eng')->nullable();
            $table->string('event_type')->nullable();
            $table->date('event_date')->nullable();
            $table->time('from_time')->nullable();
            $table->time('to_time')->nullable();
            $table->string('location_arm')->nullable();
            $table->string('location_ru')->nullable();
            $table->string('location_eng')->nullable();
            $table->text('participants_arm')->nullable();
            $table->text('participants_ru')->nullable();
            $table->text('participants_eng')->nullable();
            $table->text('overview_arm')->nullable();
            $table->text('overview_ru')->nullable();
            $table->text('overview_eng')->nullable();
            $table->string('email')->nullable();
            $table->string('active')->nullable();
            $table->string('participant_job_title_arm')->nullable();
            $table->string('participant_job_title_ru')->nullable();
            $table->string('participant_job_title_eng')->nullable();
            $table->string('pdf')->nullable();
            $table->string('photo_by_arm')->nullable();
            $table->string('photo_by_ru')->nullable();
            $table->string('photo_by_eng')->nullable();
            $table->string('	documents')->nullable();
            $table->string('	photo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LibraryUpload extends Model
{
    protected $table = 'library_upload';
    public $timestamps = true;
}

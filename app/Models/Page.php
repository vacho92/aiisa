<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'page';
    public $timestamps = true;

    public function uploads()
    {
        return $this->hasMany('App\Models\PageUpload');
    }
}

@extends('pages.home')
@section('home')
    <div>
        <h1>Add News </h1>
    </div>
    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post" action="{{url('admin/add_news')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div  class="form-group form_input arm">
                <label for="label" class="col-sm-2 control-label">Label</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="label_arm" placeholder="Label">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="label" class="col-sm-2 control-label">Label</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="label_ru" placeholder="Label">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="label" class="col-sm-2 control-label">Label</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="label_eng" placeholder="Label">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("label",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['label'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_arm" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_ru" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_eng" placeholder="Title">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("title",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['title'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="quote" class="col-sm-2 control-label">Quote</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="quote_arm">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="quote" class="col-sm-2 control-label">Quote</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="quote_ru">
                </div>
            </div>
            <div class="form-group  form_input eng" style="display: none">
                <label for="quote" class="col-sm-2 control-label">Quote</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="quote_eng">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("quote",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['quote'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="author" class="col-sm-2 control-label">Author</label>
                <div class="col-sm-5">
                    <select name="author" id="author" class="form-control">
                        @foreach($authors as $value)
                            @if($value)
                                <option  value="{{$value['id']}}">
                                    @if($value['first_name_arm'])
                                        {{$value['first_name_arm']}} {{$value['last_name_arm']}}
                                    @elseif($value['first_name_ru'])
                                        {{$value['first_name_ru']}} {{$value['last_name_ru']}}
                                    @else
                                        {{$value['first_name_eng']}} {{$value['last_name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="authors" class="col-sm-2 control-label">Authors</label>
                <div class="col-sm-5">
                    <select name="authors[]" id="authors" class="form-control" multiple>
                        @foreach($authors as $value)
                            @if($value)
                                <option  value="{{$value['id']}}">
                                    @if($value['first_name_arm'])
                                        {{$value['first_name_arm']}} {{$value['last_name_arm']}}
                                    @elseif($value['first_name_ru'])
                                        {{$value['first_name_ru']}} {{$value['last_name_ru']}}
                                    @else
                                        {{$value['first_name_eng']}} {{$value['last_name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="news_date" class="col-sm-2 control-label">News Date</label>
                <div class="col-sm-5">
                    <input type="date" class="form-control" name="news_date" id="news_date">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("news_date",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['news_date'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="secondary_link" class="col-sm-2 control-label">Secondary Link</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="secondary_link_arm">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="secondary_link" class="col-sm-2 control-label">Secondary Link</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="secondary_link_ru" >
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="secondary_link" class="col-sm-2 control-label">Secondary Link</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="secondary_link_eng">
                </div>
            </div>
            <div class="form-group ">
                <label for="link" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="link" id="link">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("link",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['link'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Select Topic</label>
                <div class="col-sm-5">
                    <select name="topics[]" id="topics" class="form-control" multiple>
                        @foreach($topic as $value)
                            @if($value)
                                <option  value="{{$value['id']}}">
                                    @if($value['name_arm']){{$value['name_arm']}}
                                    @elseif($value['name_ru']){{$value['name_ru']}}
                                    @else{{$value['name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("topic",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['topic'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="region" class="col-sm-2 control-label">Select Region</label>
                <div class="col-sm-5">
                    <select name="region[]" id="region" class="form-control" multiple>
                        @foreach($region as $value)
                            @if($value)
                                <option  value="{{$value['id']}}">
                                    @if($value['name_arm']){{$value['name_arm']}}
                                    @elseif($value['name_ru']){{$value['name_ru']}}
                                    @else{{$value['name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("region",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['region'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="program" class="col-sm-2 control-label">Select Program</label>
                <div class="col-sm-5">
                    <select name="program[]" id="program" class="form-control" multiple>
                        @foreach($project as $value)
                            @if($value)
                                <option  value="{{$value['id']}}">
                                    @if($value['name_arm']){{$value['name_arm']}}
                                    @elseif($value['name_ru']){{$value['name_ru']}}
                                    @else{{$value['name_eng']}}
                                    @endif
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("program",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span  class="text-danger">{{ session()->get('errors')['program'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-5">
                    <select name="active" class="form-control">
                        <option class="form-control" value="active"> active </option>
                        <option class="form-control" value="inactive"> inactive </option>
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("active",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['active'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">ADD</button>
                    <a href="{{url('admin/news')}}"><button type="button" class="btn btn-default">Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

@endsection
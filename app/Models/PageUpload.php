<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageUpload extends Model
{
    protected $table = 'page_upload';
    public $timestamps = true;
}

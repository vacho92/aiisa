<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryUpload extends Model
{
    protected $table = 'gallery_upload';
    public $timestamps = true;
}

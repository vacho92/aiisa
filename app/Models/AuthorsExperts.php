<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthorsExperts extends Model
{
    protected $table = 'authors_experts';
    public $timestamps = true;
}

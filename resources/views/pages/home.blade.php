@extends('layout.app')
@section('content')
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">

                        <li><a href="{{url('admin/logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" style="height: 38px" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="{{url('admin/home')}}"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="{{url('admin/experts')}}"><i class="fa fa-users"></i> Experts/Authors </a>
                        </li>
                        <li>
                            <a href="{{url('admin/regions')}}"><i class="fa fa-globe"></i> Regions </a>
                        </li>
                        <li>
                            <a href="{{url('admin/topics')}}"><i class="fa fa-table fa-fw"></i> Topics </a>
                        </li>
                        <li>
                            <a href="{{url('admin/publicationType')}}"><i class="fa fa-bullhorn fa-fw"></i> Publication Types </a>
                        </li>
                        <li>
                            <a href="{{url('admin/projects')}}"><span class="fa fa-bullhorn fa-fw"></span> Projects </a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Events <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{url('admin/events')}}">Events</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/eventsType')}}">Types</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{url('admin/news')}}"><i class="fa fa-edit fa-fw"></i> In The News </a>
                        </li>
                        <li>
                            <a href="{{url('admin/videos')}}"><i class="fa fa-edit fa-fw"></i> Videos </a>
                        </li>
                        <li>
                            <a href="{{url('admin/subscription')}}"><i class="fa fa-edit fa-fw"></i> Subscription emails </a>
                        </li>
                        <li>
                            <a href="{{url('admin/pages')}}"><i class="fa fa-edit fa-fw"></i> Pages </a>
                        </li>
                        <li>
                            <a href="{{url('admin/slider')}}"><i class="fa fa-edit fa-fw"></i> Slider </a>
                        </li>
                        <li>
                            <a href="{{url('admin/gallery')}}"><i class="fa fa-edit fa-fw"></i> Gallery </a>
                        </li>
                        <li>
                            <a href="{{url('admin/academies')}}"><i class="fa fa-edit fa-fw"></i> Academies </a>
                        </li>
                        <li>
                            <a href="{{url('admin/libraries')}}"><i class="fa fa-edit fa-fw"></i> Libraries </a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            @yield('home')
        </div>
        <!-- /#page-wrapper -->

    </div>
   @endsection
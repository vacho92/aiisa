@extends('pages.home')
@section('home')
    <div class="form-group">
        <h1>Projects</h1>
    </div>
    @if(session()->has('success'))
        <div class="form-group">
            <div class="col-sm-12 alert alert-success">
                {{ session()->get('success')}}
            </div>
        </div>
    @endif
    <div class="form-group col-sm-offset-11">
        <a href="{{url('admin/add_projects')}}"><button class="btn btn-success"> ADD </button></a>
    </div>
    <table class="table table-hover">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Created</th>
            <th>Modified</th>
            <th>Active</th>
            <th>Action</th>
        </tr>

        @foreach($project as $key => $value)
            @if($value)
                <tr>
                    <td>
                        @if($value['name_arm'])
                            {{$value['name_arm']}}
                        @elseif($value['name_ru'])
                            {{$value['name_ru']}}
                        @else
                            {{$value['name_eng']}}
                        @endif
                    </td>
                    <td>
                        @if($value['desc_arm'])
                            {{$value['desc_arm']}}
                        @elseif($value['desc_ru'])
                            {{$value['desc_ru']}}
                        @else
                            {{$value['desc_eng']}}
                        @endif
                    </td>
                    <td>
                        {{$value['created_at']}}
                    </td>
                    <td>
                        {{$value['updated_at']}}
                    </td>
                    <td>
                        @if($value['active'] === 'active')
                            <img src="{{asset('svg/tick.png')}}">
                        @else
                            <img src="{{asset('svg/cross.png')}}">
                        @endif
                    </td>
                    <td>
                        <a href='{{url("admin/edit_project/".$value['id'])}}' style="text-decoration: none">
                            <button class="btn btn-warning">Edit</button>
                        </a>
                        <a href='{{url("admin/deleteProject/".$value['id'])}}' onclick="return confirm('Are you sure?')" style="text-decoration: none">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td>
                </tr>
            @endif
        @endforeach
    </table>

@endsection
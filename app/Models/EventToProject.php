<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventToProject extends Model
{
    protected $table = 'event_project';
    public $timestamps = true;
}

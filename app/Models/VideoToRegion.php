<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoToRegion extends Model
{
    protected $table = 'video_region';
    public $timestamps = true;
}

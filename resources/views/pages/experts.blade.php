@extends('pages.home')
@section('home')
    <div class="form-group">
        <h1>Experts </h1>
    </div>
    @if(session()->has('success'))
        <div class="form-group">
            <div class="col-sm-12 alert alert-success">
                {{ session()->get('success')}}
            </div>
        </div>
    @endif
    <div class="form-group col-sm-offset-11">
        <a href="{{url('admin/add_experts')}}">
            <button class="btn btn-success"> ADD</button>
        </a>
    </div>

    <table class="table table-hover">
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>E-mail</th>
            <th>Gender</th>
            <th>Created</th>
            <th>Modified</th>
            <th>Active</th>
            <th colspan="2">Action</th>
        </tr>

        @foreach($data as $key => $value)
            @if($value)
                <tr>
                    <td>
                        @if($value['first_name_arm'])
                            {{$value['first_name_arm']}}
                        @elseif($value['first_name_ru'])
                            {{$value['first_name_ru']}}
                        @else
                            {{$value['first_name_eng']}}
                        @endif
                    </td>
                    <td>
                        @if($value['last_name_arm'])
                            {{$value['last_name_arm']}}
                        @elseif($value['last_name_ru'])
                            {{$value['last_name_ru']}}
                        @else
                            {{$value['last_name_eng']}}
                        @endif
                    </td>
                    <td>
                        {{$value['email']}}
                    </td>
                    <td>
                        {{$value['gender']}}
                    </td>
                    <td>
                        {{$value['created_at']}}
                    </td>
                    <td>
                        {{$value['updated_at']}}
                    </td>
                    <td>
                        @if($value['active'] === 'active')
                            <img src="{{asset('svg/tick.png')}}">
                        @else
                            <img src="{{asset('svg/cross.png')}}">
                        @endif
                    </td>
                    <td>
                        <a href='{{url("admin/edit_experts/".$value['id'])}}' style="text-decoration: none">
                            <button class="btn btn-warning">Edit</button>
                        </a>
                        <a href='{{url("admin/deleteExperts/".$value['id'])}}' onclick="return confirm('Are you sure?')" style="text-decoration: none">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td>
                </tr>
            @endif
        @endforeach
    </table>
    {{ $data->links() }}
@endsection
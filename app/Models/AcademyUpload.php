<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademyUpload extends Model
{
    protected $table = 'academy_upload';
    public $timestamps = true;
}

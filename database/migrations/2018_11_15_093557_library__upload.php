<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LibraryUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_upload', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('library_id')->unsigned()->index();
            $table->foreign('library_id')->references('id')->on('libraries')->onDelete('cascade');
            $table->string('files');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_upload');
    }
}

@extends('pages.home')
@section('home')
    <div>
        <h1>Add Libraries </h1>
    </div>
    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post" action="{{url('admin/add_libraries')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-5">
                    <select name="status" id="country" class="form-control">
                        <option class="form-control" value="public"> public </option>
                        <option class="form-control" value="private"> private </option>
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("status",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span  class="text-danger">{{ session()->get('errors')['status'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_arm" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_ru" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_eng" placeholder="Title">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("title",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span  class="text-danger">{{ session()->get('errors')['title'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="upload" class="col-sm-2 control-label">Browse</label>
                <div class="col-sm-5">
                    <input type="file" class="form-control" name="upload[]" id="upload" multiple>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("upload",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span  class="text-danger">{{ session()->get('errors')['upload'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">ADD</button>
                    <a href="{{url('admin/libraries')}}"><button type="button" class="btn btn-default">Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

@endsection
<?php

namespace App\Http\Controllers;

use App\Models\EventToTopic;
use App\Models\NewsToTopic;
use App\Models\Topic;
use App\Models\VideoToTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class TopicController extends Controller
{
    public function Topics()
    {
        $topic = Topic::paginate(10);
        return view('pages/topics',['data'=>$topic]);
    }
    public function addTopics(Request $request)
    {
        if ($request->isMethod('get'))
        {

            return view('pages/addTopics');
        }elseif($request->isMethod('post'))
        {
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $region = new Topic;
            $region->name_arm = $name_arm;
            $region->name_ru = $name_ru;
            $region->name_eng = $name_eng;
            $region->desc_arm = $desc_arm;
            $region->desc_ru = $desc_ru;
            $region->desc_eng = $desc_eng;
            $region->active = $active;
            $region->save();

            return redirect('admin/topics')->with('success','Successfully added');
        }

    }
    public function editTopic(Request $request,$id)
    {
        if ($request->isMethod('get')){
            $topic = Topic::find($id);
            return view('pages/editTopics',['topic'=>$topic]);
        }elseif($request->isMethod('post')){
            //dd($request->toArray());
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $topic = Topic::find($id);
            $topic->name_arm = $name_arm;
            $topic->name_ru = $name_ru;
            $topic->name_eng = $name_eng;
            $topic->desc_arm = $desc_arm;
            $topic->desc_ru = $desc_ru;
            $topic->desc_eng = $desc_eng;
            $topic->active = $active;
            $topic->save();
        }

        return redirect('admin/topics')->with('success','Successfully edited');

    }
    public function deleteTopic($id)
    {
        Topic::where('id',$id)->delete();
        VideoToTopic::where('topic_id',$id)->delete();
        NewsToTopic::where('topic_id',$id)->delete();
        EventToTopic::where('topic_id',$id)->delete();
        return Redirect::back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use App\Models\SliderUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    public function Slider()
    {
        $slider = Slider::paginate(10);
        return view('pages/slider',['data'=>$slider]);
    }
    public function addSlider(Request $request)
    {
        if ($request->isMethod('get')){

            return view('pages/addSliders');
        }elseif($request->isMethod('post')){
            $err = false;
            $errors = array();
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $link = $request->input('link');
            $active = $request->input('active');
            $page_upload = $request->file('upload');

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Menu title required";
                $err = true;
            }
            if (!$desc_arm && !$desc_ru && !$desc_eng) {
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if (!$link) {
                $errors['link'] = "Link field required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data  =  Input::except(array('_token')) ;
                $validator = Validator::make($data,$up);
                if($validator->fails()){
                    $errors['upload'] = "Upload field required and should be image";
                    $err = true;
                }
            }

            if ($err) {
                return Redirect::back()->with('errors', $errors);
            }
            $insert = new Slider;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->link = $link;
            $insert->desc_arm = $desc_arm;
            $insert->desc_ru = $desc_ru;
            $insert->desc_eng = $desc_eng;
            $insert->active = $active;
            $insert->save();
            if ($page_upload) {
                foreach ($page_upload as $file){
                    $name = rand(100,999).'_'.time();
                    $name = $name.'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new SliderUpload;
                    $insert_file->slider_id = $insert->id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/slider')->with('success', 'Successfully added');

        }

    }
    public function editSlider(Request $request,$id)
    {
        if ($request->isMethod('get')){

            $slider = Slider::where('id',$id)->with('uploads')->get()->first();
            return view('pages/editSliders',['data'=>$slider]);
        }elseif($request->isMethod('post')){
            $err = false;
            $errors = array();
            //dd($request->toArray());
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $link = $request->input('link');
            $active = $request->input('active');
            $page_upload = $request->file('upload');

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Menu title required";
                $err = true;
            }
            if (!$desc_arm && !$desc_ru && !$desc_eng) {
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if (!$link) {
                $errors['link'] = "Link field required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data  =  Input::except(array('_token')) ;
                $validator = Validator::make($data,$up);
                if($validator->fails()){
                    $errors['upload'] = "Upload field required and should be image";
                    $err = true;
                }
            }

            if ($err) {
                return Redirect::back()->with('errors', $errors);
            }
            $update = Slider::find($id);
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->link = $link;
            $update->desc_arm = $desc_arm;
            $update->desc_ru = $desc_ru;
            $update->desc_eng = $desc_eng;
            $update->active = $active;
            $update->save();
            if ($page_upload) {
                foreach ($page_upload as $file){
                    $name = rand(100,999).'_'.time();
                    $name = $name.'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new SliderUpload;
                    $insert_file->slider_id = $id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/slider')->with('success', 'Successfully edited');

        }

    }
    public function deleteSliderUpload($id)
    {
        $page = SliderUpload::find($id);
        if($page){
            if(File::delete("uploads/".$page->files)){
                SliderUpload::where('id',$id)->delete();
            };
        }
        return Redirect::back();
    }
    public function deleteSlider($id)
    {
        $page = SliderUpload::where('slider_id',$id)->get();
        if($page){
            foreach ($page as $value){
                File::delete("uploads/".$value->files);
            }
            SliderUpload::where('slider_id',$id)->delete();
        }
        Slider::where('id',$id)->delete();
        return Redirect::back();
    }
}

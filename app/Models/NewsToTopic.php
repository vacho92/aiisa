<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsToTopic extends Model
{
   protected $table = 'news_topic';
   public $timestamps = true;
}

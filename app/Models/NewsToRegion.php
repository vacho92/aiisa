<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsToRegion extends Model
{
    protected $table = 'news_region';
    public $timestamps = true;

}

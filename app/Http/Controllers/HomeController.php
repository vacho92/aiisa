<?php

namespace App\Http\Controllers;

//use App\Models\Region;
use App\Models\AuthorsExperts;
use App\Models\EmailSubscribe;
use App\Models\NewsAuthors;
use App\Models\VideoAuthors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
            if (Auth::check()) {

                return redirect('admin/home');
            }
            return view('login');
        } elseif ($request->isMethod('post')) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return redirect('admin/home');
            } else {
                Session::flash('error_login','Email or password is not correct');
                return Redirect::back();
            }
        }

    }

    public function Dashboard()
    {

        return view('pages/home');
    }
    public function Experts()
    {
        $experts = AuthorsExperts::paginate(15);

        return view('pages/experts',['data'=>$experts]);
    }
    public function EmailSubscription()
    {
        $emails = EmailSubscribe::paginate(15);
//        dd($emails->toArray());
        return view('pages/emailSubscribe',['data'=>$emails]);
    }
    public function addExperts(Request $request)
    {
        if ($request->isMethod('get')){

            return view('pages/addExperts');

        }elseif ($request->isMethod('post')){
            $err = false;
            $errors = array();
           // dd($request->toArray());
            $fname_arm = $request->input('fname_arm');
            $fname_ru = $request->input('fname_ru');
            $fname_eng = $request->input('fname_eng');
            $lname_arm = $request->input('lname_arm');
            $lname_ru = $request->input('lname_ru');
            $lname_eng = $request->input('lname_eng');
            $email = $request->input('email');
            $address_arm = $request->input('address_arm');
            $address_ru = $request->input('address_ru');
            $address_eng = $request->input('address_eng');
            $city_arm = $request->input('city_arm');
            $city_ru = $request->input('city_ru');
            $city_eng = $request->input('city_eng');
            $state_arm = $request->input('state_arm');
            $state_ru = $request->input('state_ru');
            $state_eng = $request->input('state_eng');
            $country_arm = $request->input('country_arm');
            $country_ru = $request->input('country_ru');
            $country_eng = $request->input('country_eng');
            $phone = $request->input('phone');
            $zip = $request->input('zip');
            $company_arm = $request->input('company_arm');
            $company_ru = $request->input('company_ru');
            $company_eng = $request->input('company_eng');
            $gender = $request->input('gender');
            $active = $request->input('active');

            if (!$fname_arm && !$fname_ru && !$fname_eng) {
                $errors['fname'] = "At least one of First name required";
                $err = true;
            }
            if (!$lname_arm && !$lname_ru && !$lname_eng) {
                $errors['lname'] = "At least one of Last name required";
                $err = true;
            }
            $up = array('email' => 'unique:authors_experts');
            $data = Input::except(array('_token'));
            $validator = Validator::make($data, $up);
            if (!$email) {
                $errors['email'] = "Email field required";
                $err = true;
            }
            elseif ($validator->fails()){
                $errors['email_taking'] = "Email already taken";
                $err = true;
            }

            if (!$address_arm && !$address_ru && !$address_eng) {
                $errors['address'] = "At least one of Address  required";
                $err = true;
            }

            if (!$city_arm && !$city_ru && !$city_eng) {
                $errors['city'] = "At least one of City  required";
                $err = true;
            }

             if (!$state_arm && !$state_ru && !$state_eng) {
                $errors['State'] = "At least one of State required";
                $err = true;
             }

            if (!$country_arm && !$country_ru && !$country_eng) {
                $errors['Country'] = "At least one of Country required";
                $err = true;
            }

            if (!$company_arm && !$company_ru && !$company_eng) {
                $errors['Company'] = "At least one of Company required";
                $err = true;
            }

            if (!$phone) {
                $errors['Phone'] = "Phone field required";
                $err = true;
            }
            if (!$zip) {
                $errors['Zip'] = "Zip date field required";
                $err = true;
            }

            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $insert = new AuthorsExperts;
            $insert->first_name_arm = $fname_arm;
            $insert->first_name_ru = $fname_ru;
            $insert->first_name_eng = $fname_eng;
            $insert->last_name_arm = $lname_arm;
            $insert->last_name_ru = $lname_ru;
            $insert->last_name_eng = $lname_eng;
            $insert->email = $email;
            $insert->address_arm = $address_arm;
            $insert->address_ru = $address_ru;
            $insert->address_eng = $address_eng;
            $insert->city_arm = $city_arm;
            $insert->city_ru = $city_ru;
            $insert->city_eng = $city_eng;
            $insert->state_arm = $state_arm;
            $insert->state_ru = $state_ru;
            $insert->state_eng = $state_eng;
            $insert->country_arm = $country_arm;
            $insert->country_ru = $country_ru;
            $insert->country_eng = $country_eng;
            $insert->phone = $phone;
            $insert->postal = $zip;
            $insert->company_arm = $company_arm;
            $insert->company_ru = $company_ru;
            $insert->company_eng = $company_eng;
            $insert->gender = $gender;
            $insert->active = $active;
            $insert->save();

            return redirect('admin/experts')->with('success', 'Successfully added');
        }

    }
    public function editExperts(Request $request,$id)
    {
        if ($request->isMethod('get')){

            $expert = AuthorsExperts::find($id);
//            dd($expert->toArray());
            return view('pages/editExperts',['data'=>$expert]);
        }elseif ($request->isMethod('post')){
            $err = false;
            $errors = array();
           // dd($request->toArray());
            $fname_arm = $request->input('fname_arm');
            $fname_ru = $request->input('fname_ru');
            $fname_eng = $request->input('fname_eng');
            $lname_arm = $request->input('lname_arm');
            $lname_ru = $request->input('lname_ru');
            $lname_eng = $request->input('lname_eng');
            $email = $request->input('email');
            $address_arm = $request->input('address_arm');
            $address_ru = $request->input('address_ru');
            $address_eng = $request->input('address_eng');
            $city_arm = $request->input('city_arm');
            $city_ru = $request->input('city_ru');
            $city_eng = $request->input('city_eng');
            $state_arm = $request->input('state_arm');
            $state_ru = $request->input('state_ru');
            $state_eng = $request->input('state_eng');
            $country_arm = $request->input('country_arm');
            $country_ru = $request->input('country_ru');
            $country_eng = $request->input('country_eng');
            $phone = $request->input('phone');
            $zip = $request->input('zip');
            $company_arm = $request->input('company_arm');
            $company_ru = $request->input('company_ru');
            $company_eng = $request->input('company_eng');
            $gender = $request->input('gender');
            $active = $request->input('active');

            if (!$fname_arm && !$fname_ru && !$fname_eng) {
                $errors['fname'] = "At least one of First name required";
                $err = true;
            }
            if (!$lname_arm && !$lname_ru && !$lname_eng) {
                $errors['lname'] = "At least one of Last name required";
                $err = true;
            }
            $up = array('email' => 'unique:authors_experts,id,'.$id);
            $data = Input::except(array('_token'));
            $validator = Validator::make($data, $up);
            if (!$email) {
                $errors['email'] = "Email field required";
                $err = true;
            }
            elseif ($validator->fails()){
                $errors['email_taking'] = "Email already taken";
                $err = true;
            }

            if (!$address_arm && !$address_ru && !$address_eng) {
                $errors['address'] = "At least one of Address  required";
                $err = true;
            }

            if (!$city_arm && !$city_ru && !$city_eng) {
                $errors['city'] = "At least one of City  required";
                $err = true;
            }

             if (!$state_arm && !$state_ru && !$state_eng) {
                $errors['State'] = "At least one of State required";
                $err = true;
             }

            if (!$country_arm && !$country_ru && !$country_eng) {
                $errors['Country'] = "At least one of Country required";
                $err = true;
            }

            if (!$company_arm && !$company_ru && !$company_eng) {
                $errors['Company'] = "At least one of Company required";
                $err = true;
            }

            if (!$phone) {
                $errors['Phone'] = "Phone field required";
                $err = true;
            }
            if (!$zip) {
                $errors['Zip'] = "Zip date field required";
                $err = true;
            }

            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $update = AuthorsExperts::find($id);
            $update->first_name_arm = $fname_arm;
            $update->first_name_ru = $fname_ru;
            $update->first_name_eng = $fname_eng;
            $update->last_name_arm = $lname_arm;
            $update->last_name_ru = $lname_ru;
            $update->last_name_eng = $lname_eng;
            $update->email = $email;
            $update->address_arm = $address_arm;
            $update->address_ru = $address_ru;
            $update->address_eng = $address_eng;
            $update->city_arm = $city_arm;
            $update->city_ru = $city_ru;
            $update->city_eng = $city_eng;
            $update->state_arm = $state_arm;
            $update->state_ru = $state_ru;
            $update->state_eng = $state_eng;
            $update->country_arm = $country_arm;
            $update->country_ru = $country_ru;
            $update->country_eng = $country_eng;
            $update->phone = $phone;
            $update->postal = $zip;
            $update->company_arm = $company_arm;
            $update->company_ru = $company_ru;
            $update->company_eng = $company_eng;
            $update->gender = $gender;
            $update->active = $active;
            $update->save();

            return redirect('admin/experts')->with('success', 'Successfully edited');
        }

    }
    public function deleteExperts($id)
    {
        AuthorsExperts::where('id',$id)->delete();
        NewsAuthors::where('author_id',$id)->delete();
        VideoAuthors::where('author_id',$id)->delete();
        return Redirect::back();
    }
    public function deleteEmailSubs($id)
    {
        EmailSubscribe::where('id',$id)->delete();
        return Redirect::back();
    }//
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}

@extends('layout.app')

@section('content')
    <div class="login-form">
        <form action="{{url('/login')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if(Session::has('error_login')) <p class="alert alert-danger">{{Session::get('error_login')}}</p> @endif
            <h2 class="text-center">Sign in</h2>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" name="email" placeholder="Username" required="required">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary login-btn btn-block">Sign in</button>
            </div>
        </form>
    </div>
@endsection
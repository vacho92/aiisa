<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Videos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_arm')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('title_eng')->nullable();
            $table->mediumText('content_arm')->nullable();
            $table->mediumText('content_ru')->nullable();
            $table->mediumText('content_eng')->nullable();
            $table->string('media_link_arm')->nullable();
            $table->string('media_link_ru')->nullable();
            $table->string('media_link_eng')->nullable();
            $table->string('link')->nullable();
            $table->date('date')->nullable();
            $table->string('author')->nullable();
            $table->string('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video');
    }
}

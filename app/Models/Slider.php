<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';
    public $timestamps = true;

    public function uploads()
    {
        return $this->hasMany('App\Models\SliderUpload');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AuthorsExperts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors_experts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name_arm')->nullable();
            $table->string('first_name_ru')->nullable();
            $table->string('first_name_eng')->nullable();
            $table->string('last_name_arm')->nullable();
            $table->string('last_name_ru')->nullable();
            $table->string('last_name_eng')->nullable();
            $table->string('email')->unique();
            $table->string('gender')->nullable();
            $table->string('address_arm')->nullable();
            $table->string('address_ru')->nullable();
            $table->string('address_eng')->nullable();
            $table->string('city_arm')->nullable();
            $table->string('city_ru')->nullable();
            $table->string('city_eng')->nullable();
            $table->string('state_arm')->nullable();
            $table->string('state_ru')->nullable();
            $table->string('state_eng')->nullable();
            $table->string('country_arm')->nullable();
            $table->string('country_ru')->nullable();
            $table->string('country_eng')->nullable();
            $table->string('phone')->nullable();
            $table->string('postal')->nullable();
            $table->string('company_arm')->nullable();
            $table->string('company_ru')->nullable();
            $table->string('company_eng')->nullable();
            $table->string('active')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('authors_experts');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Libraries extends Model
{
    protected $table = 'libraries';
    public $timestamps = true;

    public function uploads()
    {
        return $this->hasMany('App\Models\LibraryUpload','library_id');
    }
}

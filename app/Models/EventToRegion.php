<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventToRegion extends Model
{
    protected $table = 'event_region';
    public $timestamps = true;
}

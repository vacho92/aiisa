<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Academies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_arm')->nullable();
            $table->string('name_ru')->nullable();
            $table->string('name_eng')->nullable();
            $table->string('title_arm')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('title_eng')->nullable();
            $table->mediumText('intro_arm')->nullable();
            $table->mediumText('intro_ru')->nullable();
            $table->mediumText('intro_eng')->nullable();
            $table->mediumText('content_arm')->nullable();
            $table->mediumText('content_ru')->nullable();
            $table->mediumText('content_eng')->nullable();
            $table->string('photo_title_arm')->nullable();
            $table->string('photo_title_ru')->nullable();
            $table->string('photo_title_eng')->nullable();
            $table->string('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('academies');
    }
}

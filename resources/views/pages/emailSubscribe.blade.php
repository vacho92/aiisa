@extends('pages.home')
@section('home')
    <div class="form-group">
        <h1>Email Subscription</h1>
    </div>
    @if(session()->has('success'))
        <div class="form-group">
            <div class="col-sm-12 alert alert-success">
                {{ session()->get('success')}}
            </div>
        </div>
    @endif
    <table class="table table-hover">
        <tr>
            <th>Email</th>
            <th>Type</th>
            <th colspan="2">Action</th>
        </tr>

        @foreach($data as $key => $value)
            @if($value)
                <tr>
                    <td>
                        {{$value['email']}}
                    </td>
                    <td>
                        {{$value['type']}}
                    </td>

                    <td>
                        <a href='{{url("admin/deleteEmailSubs/".$value['id'])}}' onclick="return confirm('Are you sure?')" style="text-decoration: none">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td>
                </tr>
            @endif
        @endforeach
    </table>
    {{ $data->links() }}
@endsection
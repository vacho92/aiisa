<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class News extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label_arm')->nullable();
            $table->string('label_ru')->nullable();
            $table->string('label_eng')->nullable();
            $table->string('title_arm')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('title_eng')->nullable();
            $table->string('quote_arm')->nullable();
            $table->string('quote_ru')->nullable();
            $table->string('quote_eng')->nullable();
            $table->string('author')->nullable();
            $table->date('news_date')->nullable();
            $table->string('secondary_link_arm')->nullable();
            $table->string('secondary_link_ru')->nullable();
            $table->string('secondary_link_eng')->nullable();
            $table->string('link')->nullable();
            $table->string('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}

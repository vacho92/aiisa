@extends('pages.home')
@section('home')
    <div class="form-group">
        <h1>Pages</h1>
    </div>
    @if(session()->has('success'))
        <div class="form-group">
            <div class="col-sm-12 alert alert-success">
                {{ session()->get('success')}}
            </div>
        </div>
    @endif
    <div class="form-group col-sm-offset-11">
        <a href="{{url('admin/add_pages')}}"><button class="btn btn-success"> ADD </button></a>
    </div>
    <table class="table table-hover">
        <tr>
            <th>Title</th>
            <th>Navigation</th>
            <th colspan="2">Action</th>
        </tr>

        @foreach($data as $key => $value)
            @if($value)
                <tr>
                    <td>
                        @if($value['title_arm'])
                            {{$value['title_arm']}}
                        @elseif($value['title_ru'])
                            {{$value['title_ru']}}
                        @else
                            {{$value['title_eng']}}
                        @endif
                    </td>
                    <td>
                        {{$value['navigation']}}
                    </td>
                    <td>
                        <a href='{{url("admin/edit_pages/".$value['id'])}}' style="text-decoration: none">
                            <button class="btn btn-warning">Edit</button>
                        </a>
                        <a href='{{url("admin/deletePage/".$value['id'])}}' onclick="return confirm('Are you sure?')" style="text-decoration: none">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td>
                </tr>
            @endif
        @endforeach
    </table>
    {{$data->links()}}
@endsection
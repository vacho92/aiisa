@extends('pages.home')
@section('home')
    <div>
        <h1>Edit Sliders </h1>
    </div>
    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post" action="{{url('admin/edit_slider/'.$data['id'])}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div  class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label">  Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_arm']}}" name="title_arm" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label"> Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_ru']}}" name="title_ru" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label"> Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_eng']}}" name="title_eng" placeholder="Title">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("title",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['title'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group form_input arm">
                <label for="desc" class="col-sm-2 control-label"> Description</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="desc_arm" name="desc_arm"> {{$data['desc_arm']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('desc_arm');
                    </script>
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="desc" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="desc_ru" name="desc_ru" >{{$data['desc_ru']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('desc_ru');
                    </script>
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="desc" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="desc_eng" name="desc_eng" >{{$data['desc_eng']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('desc_eng');
                    </script>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("desc",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['desc'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group">
                <label for="link" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['link']}}" name="link" placeholder="Link">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("link",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['link'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group">
                <label for="country" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-5">
                    <select  name="active" class="form-control">
                        <option class="form-control" @if($data['active']==='active')selected @endif value="active"> active </option>
                        <option class="form-control" @if($data['active']==='inactive')selected @endif value="inactive"> inactive </option>
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("active",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['active'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="upload" class="col-sm-2 control-label">Browse</label>
                <div class="col-sm-5">
                    <input type="file" class="form-control" name="upload[]" id="upload" multiple>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("upload",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['upload'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            @if($data['uploads'])
                <div class="form-group ">
                    @foreach($data['uploads'] as $value)
                        <div class="col-sm-12">
                            <img style="width: 200px; height: 200px;" src="{{asset('uploads/'.$value['files'])}}">
                            <a href="{{url('admin/deleteSliderUpload/'.$value['id'])}}">
                                <button type="button" class="btn btn-danger">Delete</button>
                            </a>
                        </div><br>
                    @endforeach
                </div>
            @endif
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{url('admin/slider')}}"><button type="button" class="btn btn-default">Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

@endsection
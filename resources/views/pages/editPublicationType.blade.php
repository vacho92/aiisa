@extends('pages.home')
@section('home')
    <div>
        <h1>Edit Publication Type </h1>
    </div>
    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post" action="{{url('admin/edit_publicationType/'.$publication['id'])}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div  class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label"> Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$publication['name_arm']}}" name="name_arm" placeholder="Name">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label"> Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$publication['name_ru']}}" name="name_ru" placeholder="Name">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label"> Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$publication['name_eng']}}" name="name_eng" placeholder="Name">
                </div>
            </div>
            <div class="form-group form_input arm">
                <label for="lname" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="desc_arm" name="desc_arm">{{$publication['desc_arm']}} </textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('desc_arm');
                    </script>
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="lname" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="desc_ru" name="desc_ru">{{$publication['desc_ru']}} </textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('desc_ru');
                    </script>
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="lname" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="desc_eng" name="desc_eng">{{$publication['desc_eng']}} </textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('desc_eng');
                    </script>
                </div>
            </div>
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-5">
                    <select  name="active" class="form-control">
                        <option class="form-control" @if($publication['active']==='active') selected @endif value="active"> active </option>
                        <option class="form-control" @if($publication['active']==='inactive') selected @endif value="inactive"> inactive </option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{url('admin/publication')}}"><button type="button" class="btn btn-default">Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

@endsection
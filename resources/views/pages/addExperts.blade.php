@extends('pages.home')
@section('home')
    <div>
        <h1>Add Experts </h1>
    </div>

    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post" action="{{url('admin/add_experts')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div  class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="fname_arm" placeholder="First Name">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="fname_ru" placeholder="First Name">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="fname_eng" placeholder="First Name">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("fname",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['fname'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="lname" class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="lname_arm" placeholder="Last Name">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="lname" class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="lname_ru" placeholder="Last Name">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="lname" class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="lname_eng" placeholder="Last Name">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("lname",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['lname'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-5">
                    <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("email",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['email'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            @if(session()->has('errors'))
                @if(array_key_exists("email_taking",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['email_taking'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="address" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="address_arm" placeholder="Address">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="address" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="address_ru" placeholder="Address">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="address" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="address_eng" placeholder="Address">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("address",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['address'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="inputEmail3" class="col-sm-2 control-label">City</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="city_arm" id="city" placeholder="City">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="inputEmail3" class="col-sm-2 control-label">City</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="city_ru" id="city" placeholder="City">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="inputEmail3" class="col-sm-2 control-label">City</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="city_eng" id="city" placeholder="City">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("city",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['city'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="state" class="col-sm-2 control-label">State</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="state_arm" id="state" placeholder="State">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="state" class="col-sm-2 control-label">State</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="state_ru" id="state" placeholder="State">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="state" class="col-sm-2 control-label">State</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="state_eng" id="state" placeholder="State">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("State",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['State'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="country_arm" class="col-sm-2 control-label">Country</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="country_arm" id="country_arm" placeholder="Country">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="country_ru" class="col-sm-2 control-label">Country</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="country_ru" id="country_ru" placeholder="Country">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="country_eng" class="col-sm-2 control-label">Country</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="country_eng" id="country_eng" placeholder="Country">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("Country",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['Country'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="phone" class="col-sm-2 control-label">Phone Number</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("Phone",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['Phone'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group">
                <label for="zip" class="col-sm-2 control-label">Zip/Postal</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="zip" id="zip" placeholder="Zip/Postal">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("Zip",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['Zip'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="company" class="col-sm-2 control-label">Company Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="company_arm" id="company" placeholder="Company Name">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="company" class="col-sm-2 control-label">Company Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="company_ru" id="company" placeholder="Company Name">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="company" class="col-sm-2 control-label">Company Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="company_eng" id="company" placeholder="Company Name">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("Company",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['Company'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-5">
                    <select name="gender" class="form-control">
                        <option class="form-control" value="male"> Male </option>
                        <option class="form-control" value="female"> Female </option>
                    </select>
                </div>
            </div>

            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-5">
                    <select name="active" class="form-control">
                        <option class="form-control" value="active"> active </option>
                        <option class="form-control" value="inactive"> inactive </option>
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("active",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['active'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">ADD</button>
                    <a href="{{url('admin/experts')}}"><button type="button" class="btn btn-default">Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

@endsection
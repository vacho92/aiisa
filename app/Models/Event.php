<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use App\Models\Topic;
class Event extends Model
{
    protected $table = 'events';
    public $timestamps = true;

    public function topics()
    {
        return $this->belongsToMany('App\Models\Topic');
    }
    public function region()
    {
        return $this->belongsToMany('App\Models\Region');
    }
    public function project()
    {
        return $this->belongsToMany('App\Models\Project');
    }

}

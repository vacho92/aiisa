<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsToProject extends Model
{
    protected $table = 'news_project';
    public $timestamps = true;
}

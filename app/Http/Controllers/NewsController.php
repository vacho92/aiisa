<?php

namespace App\Http\Controllers;

use App\Models\AuthorsExperts;
use App\Models\News;
use App\Models\NewsAuthors;
use App\Models\NewsToProject;
use App\Models\NewsToRegion;
use App\Models\NewsToTopic;
use App\Models\Project;
use App\Models\Region;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NewsController extends Controller
{
    public function News()
    {
        $news = News::paginate(10);

        return view('pages/news',['data'=>$news]);
    }
    public function addNews(Request $request)
    {
        if ($request->isMethod('get')) {
            $authors = AuthorsExperts::get();
            $topic = Topic::get();
            $region = Region::get();
            $project = Project::get();
            return view('pages/addNews', ['authors' => $authors, 'topic' => $topic, 'region' => $region, 'project' => $project]);
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
             //dd($request->toArray());
            $label_arm = $request->input('label_arm');
            $label_ru = $request->input('label_ru');
            $label_eng = $request->input('label_eng');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $quote_arm = $request->input('quote_arm');
            $quote_ru = $request->input('quote_ru');
            $quote_eng = $request->input('quote_eng');
            $author = $request->input('author');
            $news_date = $request->input('news_date');
            $secondary_link_arm = $request->input('secondary_link_arm');
            $secondary_link_ru = $request->input('secondary_link_ru');
            $secondary_link_eng = $request->input('secondary_link_eng');
            $link = $request->input('link');
            $active = $request->input('active');
            $topics_select = $request->input('topics');
            $region_select = $request->input('region');
            $program = $request->input('program');
            $authors = $request->input('authors');

            if (!$label_arm && !$label_ru && !$label_eng) {
                $errors['label'] = "At least one of Title required";
                $err = true;
            }
            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            /*if (!$quote_arm && !$quote_ru && !$quote_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }*/
            /*if (!$secondary_link_arm && !$secondary_link_ru && !$secondary_link_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }*/
            if (!$author) {
                $errors['author'] = "Author field required";
                $err = true;
            }
            if (!$news_date) {
                $errors['news_date'] = "News date field required";
                $err = true;
            }
            if (!$link) {
                $errors['link'] = "Link  field required";
                $err = true;
            }
            /*if (!$topics_select) {
                $errors['topic'] = "Topic field required";
                $err = true;
            }
            if (!$region_select) {
                $errors['region'] = "Region field required";
                $err = true;
            }
            if (!$program) {
                $errors['program'] = "Program field required";
                $err = true;
            }*/
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $insert = new News;
            $insert->label_arm = $label_arm;
            $insert->label_ru = $label_ru;
            $insert->label_eng = $label_eng;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->quote_arm = $quote_arm;
            $insert->quote_ru = $quote_ru;
            $insert->quote_eng = $quote_eng;
            $insert->author = $author;
            $insert->news_date = $news_date;
            $insert->secondary_link_arm = $secondary_link_arm;
            $insert->secondary_link_ru = $secondary_link_ru;
            $insert->secondary_link_eng = $secondary_link_eng;
            $insert->link = $link;
            $insert->active = $active;
            $insert->save();
            if($topics_select){
                foreach ($topics_select as $value) {
                    $insert_topics = new NewsToTopic;
                    $insert_topics->topic_id = $value;
                    $insert_topics->news_id = $insert->id;
                    $insert_topics->save();
                }
            }
            if($region_select){
                foreach ($region_select as $value) {
                    $insert_region = new NewsToRegion;
                    $insert_region->region_id = $value;
                    $insert_region->news_id = $insert->id;
                    $insert_region->save();
                }
            }
            if ($program){
                foreach ($program as $value) {
                    $insert_project = new NewsToProject;
                    $insert_project->project_id = $value;
                    $insert_project->news_id = $insert->id;
                    $insert_project->save();
                }
            }
            if($authors){
                foreach ($authors as $value) {
                    $insert_project = new NewsAuthors;
                    $insert_project->author_id = $value;
                    $insert_project->news_id = $insert->id;
                    $insert_project->save();
                }
            }
            return redirect('admin/news')->with('success', 'Successfully added');
        }

    }
    public function editNews(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            $news = News::where('id',$id)
                ->with('topics')
                ->with('region')
                ->with('project')
                ->with('authors')
                ->get()->first();
            // dd($news->toArray());
            $authors = AuthorsExperts::get();
            $topic = Topic::get();
            $region = Region::get();
            $project = Project::get();

            return view('pages/editNews', [
                'authors' => $authors,
                'topic' => $topic,
                'region' => $region,
                'project' => $project,
                'data' => $news]);
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            // dd($request->toArray());
            $label_arm = $request->input('label_arm');
            $label_ru = $request->input('label_ru');
            $label_eng = $request->input('label_eng');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $quote_arm = $request->input('quote_arm');
            $quote_ru = $request->input('quote_ru');
            $quote_eng = $request->input('quote_eng');
            $author = $request->input('author');
            $news_date = $request->input('news_date');
            $secondary_link_arm = $request->input('secondary_link_arm');
            $secondary_link_ru = $request->input('secondary_link_ru');
            $secondary_link_eng = $request->input('secondary_link_eng');
            $link = $request->input('link');
            $active = $request->input('active');
            $topics_select = $request->input('topics');
            $region_select = $request->input('region');
            $program = $request->input('program');
            $authors = $request->input('authors');

            if (!$label_arm && !$label_ru && !$label_eng) {
                $errors['label'] = "At least one of Title required";
                $err = true;
            }
            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            /*if (!$quote_arm && !$quote_ru && !$quote_eng) {
               $errors['title'] = "At least one of Title required";
               $err = true;
           }*/
            /*if (!$secondary_link_arm && !$secondary_link_ru && !$secondary_link_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }*/
            if (!$author) {
                $errors['author'] = "Author field required";
                $err = true;
            }
            if (!$news_date) {
                $errors['news_date'] = "News date field required";
                $err = true;
            }
            if (!$link) {
                $errors['link'] = "Link  field required";
                $err = true;
            }
           /* if (!$topics_select) {
                $errors['topic'] = "Topic field required";
                $err = true;
            }
            if (!$region_select) {
                $errors['region'] = "Region field required";
                $err = true;
            }
            if (!$program) {
                $errors['program'] = "Program field required";
                $err = true;
            }*/
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $update = News::find($id);
            $update->label_arm = $label_arm;
            $update->label_ru = $label_ru;
            $update->label_eng = $label_eng;
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->quote_arm = $quote_arm;
            $update->quote_ru = $quote_ru;
            $update->quote_eng = $quote_eng;
            $update->author = $author;
            $update->news_date = $news_date;
            $update->secondary_link_arm = $secondary_link_arm;
            $update->secondary_link_ru = $secondary_link_ru;
            $update->secondary_link_eng = $secondary_link_eng;
            $update->link = $link;
            $update->active = $active;
            $update->save();
            NewsToTopic::where('news_id', $id)->delete();
            if($topics_select){
                foreach ($topics_select as $value) {
                    $insert_topics = new NewsToTopic;
                    $insert_topics->topic_id = $value;
                    $insert_topics->news_id = $id;
                    $insert_topics->save();
                }
            }
            NewsToRegion::where('news_id', $id)->delete();
            if($region_select){
                foreach ($region_select as $value) {
                    $insert_region = new NewsToRegion;
                    $insert_region->region_id = $value;
                    $insert_region->news_id = $id;
                    $insert_region->save();
                }
            }

            NewsToProject::where('news_id', $id)->delete();
            if ($program){
                foreach ($program as $value) {
                    $insert_project = new NewsToProject;
                    $insert_project->project_id = $value;
                    $insert_project->news_id = $id;
                    $insert_project->save();
                }
            }
            NewsAuthors::where('news_id', $id)->delete();
            if($authors){
                foreach ($authors as $value) {
                    $insert_project = new NewsAuthors;
                    $insert_project->author_id = $value;
                    $insert_project->news_id = $id;
                    $insert_project->save();
                }
            }
            return redirect('admin/news')->with('success', 'Successfully edited');
        }

    }
    public function deleteNews($id)
    {
        NewsToProject::where('news_id',$id)->delete();
        NewsToRegion::where('news_id',$id)->delete();
        NewsToTopic::where('news_id',$id)->delete();
        NewsAuthors::where('news_id',$id)->delete();
        News::where('id',$id)->delete();
        return Redirect::back();
    }
}

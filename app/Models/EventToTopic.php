<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventToTopic extends Model
{
    protected $table = 'event_topic';
    public $timestamps = true;

    public function topics()
    {
        return $this->belongsToMany('App\Models\Event','topic','id','id');
    }
}

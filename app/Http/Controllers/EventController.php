<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventToProject;
use App\Models\EventToRegion;
use App\Models\EventToTopic;
use App\Models\EventType;
use App\Models\Project;
use App\Models\Region;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class EventController extends Controller
{
    public function EventsType()
    {
        $eventType = EventType::get();
        return view('pages/eventsType', ['data' => $eventType]);
    }

    public function addEventsType(Request $request)
    {
        if ($request->isMethod('get')) {

            return view('pages/addEventsType');
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $active = $request->input('active');
            if (!$name_arm && !$name_ru && !$name_eng) {
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $insert = new EventType();
            $insert->name_arm = $name_arm;
            $insert->name_ru = $name_ru;
            $insert->name_eng = $name_eng;
            $insert->active = $active;
            $insert->save();
            return redirect('admin/eventsType')->with('success', 'Successfully added');
        }

    }

    public function editEventType(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            $eventType = EventType::find($id);
            return view('pages/editEventType', ['data' => $eventType]);
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $active = $request->input('active');
            if (!$name_arm && !$name_ru && !$name_eng) {
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $update = EventType::find($id);
            $update->name_arm = $name_arm;
            $update->name_ru = $name_ru;
            $update->name_eng = $name_eng;
            $update->active = $active;
            $update->save();
            return redirect('admin/eventsType')->with('success', 'Successfully edited');
        }

    }

    public function Events()
    {
        $event = Event::paginate(10);
        return view('pages/events', ['data' => $event]);
    }

    public function addEvents(Request $request)
    {
        if ($request->isMethod('get')) {
            $eventType = EventType::get();
            $topic = Topic::get();
            $region = Region::get();
            $project = Project::get();
            return view('pages/addEvents', ['eventType' => $eventType, 'topic' => $topic, 'region' => $region, 'project' => $project]);
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            // dd($request->toArray());
            $status = $request->input('status');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $event_types = $request->input('event_types');
            $event_date = $request->input('event_date');
            $event_from = $request->input('event_from');
            $event_to = $request->input('event_to');
            $location = $request->input('location');
            $participants_arm = $request->input('participants_arm');
            $participants_ru = $request->input('participants_ru');
            $participants_eng = $request->input('participants_eng');
            $overview_arm = $request->input('overview_arm');
            $overview_ru = $request->input('overview_ru');
            $overview_eng = $request->input('overview_eng');
            $email = $request->input('email');
            $active = $request->input('active');
            $topics_select = $request->input('topics');
            $region_select = $request->input('region');
            $program = $request->input('program');
            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            if (!$participants_arm && !$participants_ru && !$participants_eng) {
                $errors['participants'] = "At least one of Participants required";
                $err = true;
            }
            if (!$overview_arm && !$overview_ru && !$overview_eng) {
                $errors['overview'] = "At least one of Overview required";
                $err = true;
            }
            if (!$email || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = "Email field required and should be valid";
                $err = true;
            }
            if (!$status) {
                $errors['status'] = "Status field required";
                $err = true;
            }
            if (!$event_types) {
                $errors['event_types'] = "Event types field required";
                $err = true;
            }
            if (!$event_date) {
                $errors['event_date'] = "Event date field required";
                $err = true;
            }
            if (!$event_from) {
                $errors['event_from'] = "Event from field required";
                $err = true;
            }
            if (!$event_to) {
                $errors['event_to'] = "Event to field required";
                $err = true;
            }
            if (!$location) {
                $errors['location'] = "Location field required";
                $err = true;
            }
            if (!$topics_select) {
                $errors['topic'] = "Topic field required";
                $err = true;
            }
            if (!$region_select) {
                $errors['region'] = "Region field required";
                $err = true;
            }
            if (!$program) {
                $errors['program'] = "Program field required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $insert = new Event;
            $insert->status = $status;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->event_type = $event_types;
            $insert->event_date = $event_date;
            $insert->from_time = $event_from;
            $insert->to_time = $event_to;
            $insert->location = $location;
            $insert->participants_arm = $participants_arm;
            $insert->participants_ru = $participants_ru;
            $insert->participants_eng = $participants_eng;
            $insert->overview_arm = $overview_arm;
            $insert->overview_ru = $overview_ru;
            $insert->overview_eng = $overview_eng;
            $insert->email = $email;
            $insert->active = $active;
            $insert->save();
            foreach ($topics_select as $value) {
                $insert_topics = new EventToTopic();
                $insert_topics->topic_id = $value;
                $insert_topics->event_id = $insert->id;
                $insert_topics->save();
            }
            foreach ($region_select as $value) {
                $insert_region = new EventToRegion;
                $insert_region->region_id = $value;
                $insert_region->event_id = $insert->id;
                $insert_region->save();
            }
            foreach ($program as $value) {
                $insert_project = new EventToProject;
                $insert_project->project_id = $value;
                $insert_project->event_id = $insert->id;
                $insert_project->save();
            }
            return redirect('admin/events')->with('success', 'Successfully added');
        }

    }

    public function editEvents(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            $event = Event::where('id',$id)
                ->with('topics')
                ->with('region')
                ->with('project')
                ->get()->first();
           // dd($event->toArray());
            $eventType = EventType::get();
            $topic = Topic::get();
            $region = Region::get();
            $project = Project::get();

            return view('pages/editEvents', [
                'eventType' => $eventType,
                'topic' => $topic,
                'region' => $region,
                'project' => $project,
                'data' => $event]);
        } elseif ($request->isMethod('post')) {
            $err = false;
            $errors = array();
            // dd($request->toArray());
            $status = $request->input('status');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $event_types = $request->input('event_types');
            $event_date = $request->input('event_date');
            $event_from = $request->input('event_from');
            $event_to = $request->input('event_to');
            $location = $request->input('location');
            $participants_arm = $request->input('participants_arm');
            $participants_ru = $request->input('participants_ru');
            $participants_eng = $request->input('participants_eng');
            $overview_arm = $request->input('overview_arm');
            $overview_ru = $request->input('overview_ru');
            $overview_eng = $request->input('overview_eng');
            $email = $request->input('email');
            $active = $request->input('active');
            $topics_select = $request->input('topics');
            $region_select = $request->input('region');
            $program = $request->input('program');
            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            if (!$participants_arm && !$participants_ru && !$participants_eng) {
                $errors['participants'] = "At least one of Participants required";
                $err = true;
            }
            if (!$overview_arm && !$overview_ru && !$overview_eng) {
                $errors['overview'] = "At least one of Overview required";
                $err = true;
            }
            if (!$email || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = "Email field required and should be valid";
                $err = true;
            }
            if (!$status) {
                $errors['status'] = "Status field required";
                $err = true;
            }
            if (!$event_types) {
                $errors['event_types'] = "Event types field required";
                $err = true;
            }
            if (!$event_date) {
                $errors['event_date'] = "Event date field required";
                $err = true;
            }
            if (!$event_from) {
                $errors['event_from'] = "Event from field required";
                $err = true;
            }
            if (!$event_to) {
                $errors['event_to'] = "Event to field required";
                $err = true;
            }
            if (!$location) {
                $errors['location'] = "Location field required";
                $err = true;
            }
            if (!$topics_select) {
                $errors['topic'] = "Topic field required";
                $err = true;
            }
            if (!$region_select) {
                $errors['region'] = "Region field required";
                $err = true;
            }
            if (!$program) {
                $errors['program'] = "Program field required";
                $err = true;
            }
            if (!$active) {
                $errors['active'] = "Active field required";
                $err = true;
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }
            $update = Event::find($id);
            $update->status = $status;
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->event_type = $event_types;
            $update->event_date = $event_date;
            $update->from_time = $event_from;
            $update->to_time = $event_to;
            $update->location = $location;
            $update->participants_arm = $participants_arm;
            $update->participants_ru = $participants_ru;
            $update->participants_eng = $participants_eng;
            $update->overview_arm = $overview_arm;
            $update->overview_ru = $overview_ru;
            $update->overview_eng = $overview_eng;
            $update->email = $email;
            $update->active = $active;
            $update->save();
            EventToTopic::where('event_id', $id)->delete();
            foreach ($topics_select as $value) {
                $insert_topics = new EventToTopic();
                $insert_topics->topic_id = $value;
                $insert_topics->event_id = $update->id;
                $insert_topics->save();
            }
            EventToRegion::where('event_id', $id)->delete();
            foreach ($region_select as $value) {
                $insert_region = new EventToRegion;
                $insert_region->region_id = $value;
                $insert_region->event_id = $update->id;
                $insert_region->save();
            }
            EventToProject::where('event_id', $id)->delete();
            foreach ($program as $value) {
                $insert_project = new EventToProject;
                $insert_project->project_id = $value;
                $insert_project->event_id = $update->id;
                $insert_project->save();
            }
            return redirect('admin/events')->with('success', 'Successfully edited');
        }

    }

    public function deleteEvent($id)
    {
        Event::where('id',$id)->delete();
        return Redirect::back();
    }
    public function deleteEventType($id)
    {
        EventType::where('id',$id)->delete();
        return Redirect::back();
    }
}

@extends('pages.home')
@section('home')
    <div>
        <h1>Add Academies </h1>
    </div>
    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post" action="{{url('admin/add_academies')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div  class="form-group form_input arm">
                <label for="name" class="col-sm-2 control-label"> Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="name_arm" placeholder="Name">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="title_ru" placeholder="Name">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  name="title_eng" placeholder="Name">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("name",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['name'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label"> Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_arm" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_ru" placeholder="Title">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="title_eng" placeholder="Title">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("title",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['title'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group form_input arm">
                <label for="intro" class="col-sm-2 control-label"> Intro text</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="intro_arm" name="intro_arm"></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('intro_arm');
                    </script>
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="intro" class="col-sm-2 control-label">Intro text</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="intro_ru" name="intro_ru"></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('intro_ru');
                    </script>
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="intro" class="col-sm-2 control-label">Intro text</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="intro_eng" name="intro_eng"></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('intro_eng');
                    </script>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("intro",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['intro'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group form_input arm">
                <label for="content" class="col-sm-2 control-label"> Content</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="content_arm" name="content_arm"></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('content_arm');
                    </script>
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="content" class="col-sm-2 control-label">Content</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="content_ru" name="content_ru"></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('content_ru');
                    </script>
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="content" class="col-sm-2 control-label">Content</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="content_eng" name="content_eng"></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('content_eng');
                    </script>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("content",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['content'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group form_input arm">
                <label for="photo_title" class="col-sm-2 control-label"> Photo title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="photo_title_arm" placeholder="Photo title">
                </div>
            </div>
            <div  class="form-group form_input ru" style="display: none">
                <label for="photo_title" class="col-sm-2 control-label"> Photo title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="photo_title_ru" placeholder="Photo title">
                </div>
            </div>
            <div  class="form-group form_input eng" style="display: none">
                <label for="photo_title" class="col-sm-2 control-label"> Photo title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="photo_title_eng" placeholder="Photo title">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("photo_title",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['photo_title'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group">
                <label for="photo" class="col-sm-2 control-label"> Photo </label>
                <div class="col-sm-5">
                    <input type="file" class="form-control" name="photo" placeholder="Photo">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("photo",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['photo'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-5">
                    <select name="active" class="form-control">
                        <option class="form-control" value="active"> active </option>
                        <option class="form-control" value="inactive"> inactive </option>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="upload" class="col-sm-2 control-label">Browse</label>
                <div class="col-sm-5">
                    <input type="file" class="form-control" name="upload[]" id="upload" multiple>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("upload",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['upload'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">ADD</button>
                    <a href="{{url('admin/academies')}}"><button type="button" class="btn btn-default">Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

@endsection
<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\PageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function Pages()
    {
        $page = Page::paginate(10);
        return view('pages/pages', ['data' => $page]);
    }

    public function addPages(Request $request)
    {
        if ($request->isMethod('get')) {

            return view('pages/addPages');
        } elseif ($request->isMethod('post')) {

            // dd($request->toArray());
            $err = false;
            $errors = array();
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $content_arm = $request->input('content_arm');
            $content_ru = $request->input('content_ru');
            $content_eng = $request->input('content_eng');
            $navigation = $request->input('navigation');
            $page_upload = $request->file('page_upload');
            $type = $request->input('type');
            $url = $request->input('url');

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Menu title required";
                $err = true;
            }
            if (!$content_arm && !$content_ru && !$content_eng) {
                $errors['content'] = "At least one of Content required";
                $err = true;
            }
            if (!$navigation) {
                $errors['navigation'] = "Navigation field required";
                $err = true;
            }
            if (!$type) {
                $errors['type'] = "Type field required";
                $err = true;
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field required and should be image";
                    $err = true;
                }
            }

            if ($err) {
                return Redirect::back()->with('errors', $errors);
            }
            $insert = new Page;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->navigation = $navigation;
            $insert->content_arm = $content_arm;
            $insert->content_ru = $content_ru;
            $insert->content_eng = $content_eng;
            $insert->type = $type;
            $insert->url = $url;
            $insert->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new PageUpload;
                    $insert_file->page_id = $insert->id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/pages')->with('success', 'Successfully added');

        }

    }

    public function editPages(Request $request, $id)
    {
        if ($request->isMethod('get')) {

            $pages = Page::where('id', $id)->with('uploads')->get()->first();

            return view('pages/editPages', ['data' => $pages]);
        } elseif ($request->isMethod('post')) {

            // dd($request->toArray());
            $err = false;
            $errors = array();
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $content_arm = $request->input('content_arm');
            $content_ru = $request->input('content_ru');
            $content_eng = $request->input('content_eng');
            $navigation = $request->input('navigation');
            $page_upload = $request->file('page_upload');
            $type = $request->input('type');
            $url = $request->input('url');

            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Menu title required";
                $err = true;
            }
            if (!$content_arm && !$content_ru && !$content_eng) {
                $errors['content'] = "At least one of Content required";
                $err = true;
            }
            if (!$navigation) {
                $errors['navigation'] = "Navigation field required";
                $err = true;
            }
            if (!$type) {
                $errors['type'] = "Type field required";
                $err = true;
            }

            if ($page_upload) {
                $up = array('page_upload.*' => 'image|mimes:jpg,png');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field required and should be image";
                    $err = true;
                }
            }

            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }

            $update = Page::find($id);
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->navigation = $navigation;
            $update->content_arm = $content_arm;
            $update->content_ru = $content_ru;
            $update->content_eng = $content_eng;
            $update->type = $type;
            $update->url = $url;
            $update->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new PageUpload;
                    $insert_file->page_id = $update->id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/pages')->with('success', 'Successfully edited');

        }

    }

    public function deletePageUpload($id)
    {
        $page = PageUpload::find($id);
        if ($page) {
            if (File::delete("uploads/" . $page->files)) {
                PageUpload::where('id', $id)->delete();
            };
        }
        return Redirect::back();
    }

    public function deletePage($id)
    {
        $page = PageUpload::where('page_id', $id)->get();
        if ($page) {
            foreach ($page as $value) {
                File::delete("uploads/" . $value->files);
            }
            PageUpload::where('page_id', $id)->delete();
        }
        Page::where('id', $id)->delete();
        return Redirect::back();
    }
}

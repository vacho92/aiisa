<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topic';
    public $timestamps = true;

    public function topics()
    {
        return $this->belongsToMany('App\Models\Event','event_topic','event_id');
    }
}

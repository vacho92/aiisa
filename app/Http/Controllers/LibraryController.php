<?php

namespace App\Http\Controllers;

use App\Models\Libraries;
use App\Models\LibraryUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class LibraryController extends Controller
{
    public function Library()
    {
        $library = Libraries::get();
        return view('pages/library', ['data' => $library]);
    }

    public function addLibrary(Request $request)
    {
        if ($request->isMethod('get')) {

            return view('pages/addLibrary');
        } elseif ($request->isMethod('post')) {
            // dd($request->toArray());
            $err = false;
            $errors = array();
            $status = $request->input('status');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $page_upload = $request->file('upload');
            $name = null;
            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            if (!$status) {
                $errors['status'] = "Status field required";
                $err = true;
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'mimes:pdf,docx,txt');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field  should be image";
                    $err = true;
                }
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }

            $insert = new Libraries;
            $insert->status = $status;
            $insert->title_arm = $title_arm;
            $insert->title_ru = $title_ru;
            $insert->title_eng = $title_eng;
            $insert->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new LibraryUpload;
                    $insert_file->library_id = $insert->id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/libraries')->with('success', 'Successfully added');
        }

    }

    public function editLibrary(Request $request, $id)
    {
        if ($request->isMethod('get')) {

            $library = Libraries::where('id', $id)->with('uploads')->get()->first();
            //dd($library->toArray());
            return view('pages/editLibrary', ['data' => $library]);
        } elseif ($request->isMethod('post')) {
            // dd($request->toArray());
            $err = false;
            $errors = array();
            $status = $request->input('status');
            $title_arm = $request->input('title_arm');
            $title_ru = $request->input('title_ru');
            $title_eng = $request->input('title_eng');
            $page_upload = $request->file('upload');
            $name = null;
            if (!$title_arm && !$title_ru && !$title_eng) {
                $errors['title'] = "At least one of Title required";
                $err = true;
            }
            if (!$status) {
                $errors['status'] = "Status field required";
                $err = true;
            }
            if ($page_upload) {
                $up = array('page_upload.*' => 'mimes:pdf,docx,txt');
                $data = Input::except(array('_token'));
                $validator = Validator::make($data, $up);
                if ($validator->fails()) {
                    $errors['upload'] = "Upload field  should be image";
                    $err = true;
                }
            }
            if ($err) {

                return Redirect::back()->with('errors', $errors);
            }

            $update = Libraries::find($id);
            $update->status = $status;
            $update->title_arm = $title_arm;
            $update->title_ru = $title_ru;
            $update->title_eng = $title_eng;
            $update->save();
            if ($page_upload) {
                foreach ($page_upload as $file) {
                    $name = rand(100, 999) . '_' . time();
                    $name = $name . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads');
                    $file->move($destinationPath, $name);
                    $insert_file = new LibraryUpload;
                    $insert_file->library_id = $id;
                    $insert_file->files = $name;
                    $insert_file->save();
                }

            }

            return redirect('admin/libraries')->with('success', 'Successfully edited');
        }

    }

    public function deleteLibraryUpload($id)
    {
        $page = LibraryUpload::find($id);
        if ($page) {
            if (File::delete("uploads/" . $page->files)) {
                LibraryUpload::where('id', $id)->delete();
            };
        }
        return Redirect::back();
    }

    public function deleteLibrary($id)
    {
        $libraryUpload = LibraryUpload::where('library_id', $id)->get();
        if ($libraryUpload) {
            foreach ($libraryUpload as $value) {
                File::delete("uploads/" . $value->files);
            }
            LibraryUpload::where('library_id', $id)->delete();
        }

        Libraries::where('id', $id)->delete();

        return Redirect::back();
    }

    public function downloadLibraryUpload($id)
    {
        $libraryUpload = LibraryUpload::find($id);

        //$download = Storage::url($libraryUpload->files);

        return Storage::download(asset("uploads/$libraryUpload->files"));
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoToProject extends Model
{
    protected $table = 'video_project';
    public $timestamps = true;
}

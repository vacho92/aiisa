<?php

namespace App\Http\Controllers;

use App\Models\EventToProject;
use App\Models\NewsToProject;
use App\Models\Project;
use App\Models\VideoToProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProjectController extends Controller
{
    public function projects()
    {
        $project = Project::get();
        return view('pages/projects',['project'=>$project]);
    }
    public function addProjects(Request $request)
    {
        if ($request->isMethod('get')){

            return view('pages/addProjects');
        }elseif($request->isMethod('post')){
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $project = new Project;
            $project->name_arm = $name_arm;
            $project->name_ru = $name_ru;
            $project->name_eng = $name_eng;
            $project->desc_arm = $desc_arm;
            $project->desc_ru = $desc_ru;
            $project->desc_eng = $desc_eng;
            $project->active = $active;
            $project->save();
            return redirect('admin/projects')->with('success','Successfully added');
        }

    }
    public function editProject(Request $request,$id)
    {
        if ($request->isMethod('get')){
            $project = Project::find($id);
            return view('pages/editProject',['project'=>$project]);
        }elseif($request->isMethod('post')){
            //dd($request->toArray());
            $err = false;
            $errors = array();
            $name_arm = $request->input('name_arm');
            $name_ru = $request->input('name_ru');
            $name_eng = $request->input('name_eng');
            $desc_arm = $request->input('desc_arm');
            $desc_ru = $request->input('desc_ru');
            $desc_eng = $request->input('desc_eng');
            $active = $request->input('active');
            if(!$name_arm && !$name_ru && !$name_eng){
                $errors['name'] = "At least one of Name required";
                $err = true;
            }
            if(!$desc_arm && !$desc_ru && !$desc_eng){
                $errors['desc'] = "At least one of Description required";
                $err = true;
            }
            if(!$active){
                $errors['active'] = "Active field required";
                $err = true;
            }
            if($err){

                return Redirect::back()->with('errors',$errors);
            }
            $project = Project::find($id);
            //dd($region);
            $project->name_arm = $name_arm;
            $project->name_ru = $name_ru;
            $project->name_eng = $name_eng;
            $project->desc_arm = $desc_arm;
            $project->desc_ru = $desc_ru;
            $project->desc_eng = $desc_eng;
            $project->active = $active;
            $project->save();
        }

        return redirect('admin/projects')->with('success','Successfully edited');

    }
    public function deleteProject($id)
    {
        Project::where('id',$id)->delete();
        VideoToProject::where('project_id',$id)->delete();
        NewsToProject::where('project_id',$id)->delete();
        EventToProject::where('project_id',$id)->delete();
        return Redirect::back();
    }
}

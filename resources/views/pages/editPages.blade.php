@extends('pages.home')
@section('home')
    <div>
        <h1>Edit Pages </h1>
    </div>
    <div class="col-md-5 col-md-offset-3">
        <ul class="nav nav-tabs nav-justified col-md-5">
            <li class="nav-item active">
                <a class="nav-link addFormTab" data-id="arm" href="#">Armenian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="ru" href="#">Russian</a>
            </li>
            <li class="nav-item">
                <a class="nav-link addFormTab" data-id="eng" href="#">English</a>
            </li>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="">
        <form class="form-horizontal col-md-offset-3" role="form" method="post" enctype="multipart/form-data"
              action="{{url('admin/edit_pages/'.$data['id'])}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group form_input arm">
                <label for="fname" class="col-sm-2 control-label"> Menu Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_arm']}}" name="title_arm"
                           placeholder="Title">
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Menu Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_ru']}}" name="title_ru"
                           placeholder="Title">
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="fname" class="col-sm-2 control-label">Menu Title</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['title_eng']}}" name="title_eng"
                           placeholder="Title">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("title",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['title'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="navigation" class="col-sm-2 control-label">Navigation</label>
                <div class="col-sm-5">
                    <input type="radio" @if($data['navigation']==='Top') checked @endif name="navigation"
                           id="navigation" value="Top">Top<br>
                    <input type="radio" @if($data['navigation']==='Bottom') checked @endif name="navigation"
                           id="navigation" value="Bottom">Bottom<br>
                    <input type="radio" @if($data['navigation']==='Publication') checked @endif name="navigation"
                           id="navigation" value="Publication">Publication<br>
                    <input type="radio" @if($data['navigation']==='Do_not_publish') checked @endif name="navigation"
                           id="navigation" value="Do_not_publish">Do not publish<br>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("navigation",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['navigation'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group form_input arm">
                <label for="content" class="col-sm-2 control-label"> Content</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="content_arm" name="content_arm">{{$data['content_arm']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('content_arm');
                    </script>
                </div>
            </div>
            <div class="form-group form_input ru" style="display: none">
                <label for="content" class="col-sm-2 control-label">Content</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="content_ru" name="content_ru">{{$data['content_ru']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('content_ru');
                    </script>
                </div>
            </div>
            <div class="form-group form_input eng" style="display: none">
                <label for="content" class="col-sm-2 control-label">Content</label>
                <div class="col-sm-5">
                    <textarea type="text" class="form-control" id="content_eng" name="content_eng">{{$data['content_eng']}}</textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('content_eng');
                    </script>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("content",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['content'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div  class="form-group">
                <label for="content" class="col-sm-2 control-label">Url</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="{{$data['url']}}" name="url">
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("url",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['url'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="country" class="col-sm-2 control-label">Type</label>
                <div class="col-sm-5">
                    <select name="type" class="form-control">
                        <option class="form-control" @if($data['type']==='dynamic') selected @endif value="dynamic"> dynamic </option>
                        <option class="form-control" @if($data['type']==='static') selected @endif value="static"> static </option>
                    </select>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("type",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['type'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            <div class="form-group ">
                <label for="upload" class="col-sm-2 control-label">Browse</label>
                <div class="col-sm-5">
                    <input type="file" class="form-control" name="page_upload[]" id="upload" multiple>
                </div>
            </div>
            @if(session()->has('errors'))
                @if(array_key_exists("upload",session()->get('errors')))
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <span class="text-danger">{{ session()->get('errors')['upload'] }}</span>
                        </div>
                    </div>
                @endif
            @endif
            @if($data['uploads'])
                <div class="form-group ">
                    @foreach($data['uploads'] as $value)
                        <div class="col-sm-12">
                            <img style="width: 200px; height: 200px;" src="{{asset('uploads/'.$value['files'])}}">
                            <a href="{{url('admin/deletePageUpload/'.$value['id'])}}">
                                <button type="button" class="btn btn-danger">Delete</button>
                            </a>
                        </div><br>
                    @endforeach
                </div>
            @endif
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{url('admin/pages')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                </div>
            </div>
        </form>
    </div>

@endsection
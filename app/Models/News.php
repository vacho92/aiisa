<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    public $timestamps = true;

    public function topics()
    {
        return $this->belongsToMany('App\Models\Topic');
    }
    public function region()
    {
        return $this->belongsToMany('App\Models\Region');
    }
    public function project()
    {
        return $this->belongsToMany('App\Models\Project');
    }
    public function authors()
    {
        return $this->belongsToMany('App\Models\AuthorsExperts','news_author','news_id','author_id');
    }
}
